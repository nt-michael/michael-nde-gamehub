
import RoutesModule from './configs/RoutesModule'

function App() {

  return (
    <>
      <RoutesModule />
    </>
  )
}

export default App

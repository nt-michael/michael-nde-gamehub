/* eslint-disable @typescript-eslint/no-explicit-any */
import Axios from "axios";
import { API_KEY, BASE_API_URL } from ".";

const axios = Axios.create({ baseURL: BASE_API_URL });

const authRequestInterceptor = (config: any) => {
  config.headers = { ...config.headers };
  config.headers.Accept = "application/json";
  config.headers["Content-Type"] = "application/json";
  config.url += config.url.includes("?")
    ? `&key=${API_KEY}`
    : `?key=${API_KEY}`;
  return config;
};
axios.interceptors.request.use(authRequestInterceptor);

export default axios;

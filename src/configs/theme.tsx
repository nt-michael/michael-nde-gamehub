import { createTheme } from "@mui/material";


const theme = createTheme({
    palette: {
        primary: {
            main: '#2401DB',
            contrastText:"white"
        },
        secondary: {
            main: '#2401DB',
        },
        info: {
            main: '#0052cc',
        },
      
    },
});

export default theme;


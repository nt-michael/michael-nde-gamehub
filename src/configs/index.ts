export const BASE_API_URL = import.meta.env.VITE_APP_BASE_API_URL;
export const API_KEY = import.meta.env.VITE_APP_API_KEY;

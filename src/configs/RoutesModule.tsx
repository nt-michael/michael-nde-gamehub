import { BrowserRouter, Routes as CustomRoutes, Navigate, Route } from "react-router-dom";
import appConfigs from "../apps/appConfigs";

const RoutesModule = () => {
  return (
    <BrowserRouter>
      <CustomRoutes>
        <Route path="/" element={<Navigate to={'app/games'} />} />
        {appConfigs.routes.map(
          (route, index) => (
            <Route key={index} path={route.path} element={route.element} />
          )
        )}
      </CustomRoutes>
    </BrowserRouter>
  );
};

export default RoutesModule;

import { Link, useNavigate, useParams } from 'react-router-dom';
import {
    AppBar,
    Typography,
    Box,
    Button,
    styled,
    Container,
    InputBase,
    IconButton,
    Divider,
    Drawer,
    List,
    ListItem,
    ListItemButton,
    ListItemText,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import MenuIcon from '@mui/icons-material/Menu';
// import './navbar.scss';
import { useState } from 'react';
import useCombinedStores from '../../apps/Store';
// import { useTheme  } from '@emotion/react';

const Navbar = () => {
    const { filterStore } = useCombinedStores();
    const navigate = useNavigate();
    const { gameid } = useParams();
    const links = ['HOME'];
    // const theme = useTheme();
    const [open, setOpen] = useState(false);
    const drawerWidth = 240;
    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const Search = styled('div')(({ theme }) => ({
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: "transparent",
        '&:hover': {
            backgroundColor: "transparent",
        },
        marginLeft: 0,
        width: '100%',
        border: "1px solid #8B8B8B",
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    }));

    const SearchIconWrapper = styled('div')(({ theme }) => ({
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }));

    const StyledInputBase = styled(InputBase)(({ theme }) => ({
        color: 'inherit',
        width: '100%',
        '& .MuiInputBase-input': {
            padding: theme.spacing(1, 1, 1, 0),
            paddingLeft: `calc(1em + ${theme.spacing(4)})`,
            transition: theme.transitions.create('width'),
            [theme.breakpoints.up('sm')]: {
                width: '12ch',
                '&:focus': {
                    width: '20ch',
                },
            },
        },
    }));

    const searchFunc = (text: string) => {
        if (text.length === 0) {
            filterStore.filter("search", null as never);
        }
        if ((text.length > 3) || (text.length === 3)) {
            filterStore.filter("search", text as never);
        }

        if (gameid) navigate(`/app/games`);
    }

    return (
        <AppBar position="static" className='bg-gradient-to-r from-black to-[#6650A5]'>
            <Container>
                <Box className='flex justify-between' >
                    <Box className="flex items-center ">
                        <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={handleDrawerOpen}
                                edge="start"
                                sx={{ mr: 2, ...(open && { display: 'none' }) }}
                            >
                                <MenuIcon />
                            </IconButton>
                        </Box>
                        <Link to="/" className='no-underline'>
                            <Typography variant="h6" noWrap component="div" sx={{ mr: 2, display: { xs: 'block', md: 'flex' } }}>
                                <img src="/public/logos/logo.svg" alt="GameHub Logo" width="62" height="71" />
                            </Typography>
                        </Link>
                        <Box sx={{ display: { xs: 'none', md: 'flex' }, gap: "1rem" }}>
                            <Link to="/" className='no-underline'>
                                <Button color="inherit">Home</Button>
                            </Link>
                        </Box>
                    </Box>
                    <Box className="flex gap-2 items-center">
                        <Search>
                            <SearchIconWrapper>
                                <SearchIcon />
                            </SearchIconWrapper>
                            <StyledInputBase
                                placeholder="Search…"
                                inputProps={{ 'aria-label': 'search' }}
                                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                                onKeyDown={(e: any) => {
                                    if (e.key === "Enter") {
                                        searchFunc(e.target?.value ?? '');
                                    }

                                }}
                            />
                        </Search>
                        {/* <DarkModeToggle /> */}
                    </Box>
                </Box>
            </Container>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    },
                }}
                variant="persistent"
                anchor="left"
                open={open}
            >
                <DrawerHeader>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <List>
                    {links.map((text) => (
                        <ListItem key={text} disablePadding>
                            <ListItemButton>
                                <ListItemText primary={text} />
                            </ListItemButton>
                        </ListItem>
                    ))}
                </List>
            </Drawer>
        </AppBar>
    )
}
export default Navbar
import { ComponentType, useEffect, useState } from 'react';
import './with-loading.scss';

import Navbar from '../Navbar/Navbar';

interface WithLoadingProps {
    initialLoadingDuration?: number;
}

function withLoading<T extends object>(WrappedComponent: ComponentType<T & WithLoadingProps>) {
    return function WithLoadingComponent(props: T) {
        const [isLoading, setLoading] = useState(true);

        const setLoadingAndReset = (value: boolean) => {
            setLoading(value);
            setTimeout(() => setLoading(false), 700); // Simulating loading for 2 seconds
        };

        useEffect(() => {
            setLoadingAndReset(true);
            document.body.classList.add('dark');
        }, [])

        const LoadingPlaceholder = () => {
            // Customize this component to match your UI and loading indicator preferences
            return <div className="loading-placeholder">Loading...</div>;
        };

        return isLoading ? <div className='layout-page'><LoadingPlaceholder /></div> :
            <div className='layout-page'>
                <Navbar />
                <WrappedComponent {...props as T} isLoading={isLoading} setLoading={setLoadingAndReset} />
            </div>;
    };
}

export default withLoading
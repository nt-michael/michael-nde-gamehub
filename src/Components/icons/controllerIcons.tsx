import { SvgIcon } from '@mui/material';
import { SvgIconProps } from '@mui/material/SvgIcon';

const ControllerIcon = (props: SvgIconProps) => {
  return (
    <SvgIcon viewBox="0 0 24 24" {...props}>
      <path d="M11.66 13.4c.43.14.43.47 0 .61l-7 3.8c-.43.23-.88.11-1.17-.27-.29-.38-.15-.91.27-1.17l7-3.8c.43-.23.43-.61 0-.84L4.61 8.6c-.43-.23-.43-.61 0-.84.43-.23.92-.23 1.35 0l7.6 4.2c.43.23.43.61 0 .84L5.37 14.4c-.43.23-.43.61 0 .84.43.23.92.23 1.35 0z" />
    </SvgIcon>
  );
};

export default ControllerIcon;
import { create } from "zustand";
import { GameDataDTO } from "../types";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const useGameStore = create<GameDataDTO>((set) => ({
  page: 1,
  count: null,
  previous: null,
  next: null,
  results: [],
  init: (res: GameDataDTO) => {
    const regex = /&page=(\d+)/;
    if (res.next) {
      const match = res.next.match(regex);
      const newState = { ...res, page: Number(match![1]) };
      set(newState);
    } else {
      set({ ...res, page: null });
    }
    //
  },
  loadMore: (res: GameDataDTO) => {
    const regex = /&page=(\d+)/;
    if (res.next) {
      const match = res.next.match(regex);
      set((state) => ({
        ...res,
        page: Number(match![1]),
        results: [...(state?.results as []), ...(res?.results as [])],
      }));
    } else {
      set((state) => ({
        ...res,
        page: null,
        results: [...(state.results as []), ...(res.results as [])],
      }));
    }
  },
}));

export default useGameStore;

import { create } from "zustand";
import { OrderDataDTO } from "../types";

const initOptions: { key: string; value: string }[] = [
  { key: "Name", value: "name" },
  { key: "Released", value: "released" },
  { key: "Added", value: "added" },
  { key: "Created", value: "created" },
  { key: "Updated", value: "updated" },
  { key: "Rating", value: "rating" },
  { key: "Metacritic", value: "metacritic" },
];
const useOrderStore = create<OrderDataDTO>((set) => ({
  options: initOptions,
  sort: "asc",
  sortBy: () =>
    set((state) => {
      if (state.sort === "desc") {
        return { options: initOptions, sort: "asc" };
      } else {
        return {
          options: state.options.map((option) => ({
            key: option.key,
            value: `-${option.value}`,
          })),
          sort: "desc",
        };
      }
    }),
}));

export default useOrderStore;

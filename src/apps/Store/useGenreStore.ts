import { create } from "zustand";
import { GenreDTO } from "../types";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const useGenreStore = create<GenreDTO>((set) => ({
  count: null,
  previous: null,
  next: null,
  results: null,
  init: (res) => {
    set(res);
  },
}));

export default useGenreStore;

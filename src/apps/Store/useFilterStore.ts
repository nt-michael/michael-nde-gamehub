import { create } from "zustand";
import { FilterParamDTO } from "../types";

const useFilterStore = create<FilterParamDTO>((set) => ({
  search: null,
  ordering: null,
  genres: null,
  platforms: null,
  filter: (
    param: "search" | "ordering" | "genres" | "platforms",
    value: never
  ) => {
    switch (param) {
      case "search":
        set({ search: (value as string) ? (value as string) : null });
        break;
      case "ordering":
        set({
          ordering: value as
            | "name"
            | "released"
            | "added"
            | "created"
            | "updated"
            | "rating"
            | "metacritic"
            | "-name"
            | "-released"
            | "-added"
            | "-created"
            | "-updated"
            | "-rating"
            | "-metacritic"
            | null,
        });
        break;
      case "genres":
        set({
          genres:
            (value as string[] | number[]).length > 0
              ? (value as string[])
              : null,
        });
        break;
      case "platforms":
        set({
          platforms:
            (value as string[] | number[]).length > 0
              ? (value as string[])
              : null,
        });
        break;

      default:
        break;
    }
  },
}));

export default useFilterStore;

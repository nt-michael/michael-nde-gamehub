import useFilterStore from "./useFilterStore";
import useGameStore from "./useGameStore";
import useGenreStore from "./useGenreStore";
import useOrderStore from "./useOrderStore";
import usePlatformStore from "./usePlatformStore";

const useCombinedStores = () => {
  const filterStore = useFilterStore();
  const gameStore = useGameStore();
  const genreStore = useGenreStore();
  const orderStore = useOrderStore();
  const platformStore = usePlatformStore();

  return {
    filterStore,
    gameStore,
    genreStore,
    orderStore,
    platformStore,
  };
};
export default useCombinedStores;

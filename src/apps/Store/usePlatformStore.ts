import { create } from "zustand";
import { PlatformDTO } from "../types";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const usePlatformStore = create<PlatformDTO>((set) => ({
  count: null,
  previous: null,
  next: null,
  results: null,
  init: (res) => {
    set(res);
  },
}));

export default usePlatformStore;

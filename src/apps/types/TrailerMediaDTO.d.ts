export interface TrailerMediaDTO {
  count: number;
  next: string;
  previous: string;
  results: {
    id: number;
    name: string;
    preview: string;
    data: {
      480: string;
      max: string;
    };
  }[];
}

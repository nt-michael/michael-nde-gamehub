export interface PlatformDTO {
  init(res: PlatformDTO): void;
  count: number | null;
  next: string | null;
  previous: string | null;
  results:
    | {
        id: number;
        name: string;
        slug: string;
        platforms: {
          id: number;
          name: string;
          slug: string;
          games_count: number;
          image_background: string;
          image: string;
          year_start: number;
          year_end: number;
        }[];
      }[]
    | null;
}

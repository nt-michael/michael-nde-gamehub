export interface OrderDataDTO {
  sortBy(): void;
  options: { key: string; value: string }[];
  sort: "asc" | "desc";
}

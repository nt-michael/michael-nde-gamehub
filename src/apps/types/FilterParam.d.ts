export interface FilterParamDTO {
  search: string | null;
  ordering:
    | "name"
    | "released"
    | "added"
    | "created"
    | "updated"
    | "rating"
    | "metacritic"
    | "-name"
    | "-released"
    | "-added"
    | "-created"
    | "-updated"
    | "-rating"
    | "-metacritic"
    | null;
  genres: string[] | number[] | null;
  platforms: string[] | number[] | null;
  filter: (
    param: "search" | "ordering" | "genres" | "platforms",
    value: never
  ) => void;
}

export interface FilterPayloadDTO {
  search: string | null;
  ordering:
    | "name"
    | "released"
    | "added"
    | "created"
    | "updated"
    | "rating"
    | "metacritic"
    | "-name"
    | "-released"
    | "-added"
    | "-created"
    | "-updated"
    | "-rating"
    | "-metacritic"
    | null;
  genres: string[] | number[] | null;
  platforms: string[] | null;
}

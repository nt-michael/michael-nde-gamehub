export interface GameDataDTO {
  page: number | null;
  init(res: GameDataDTO): void;
  loadMore(res: GameDataDTO): void;
  count: number | null;
  next: string | null;
  previous: string | null;
  results:
    | {
        id: number;
        slug: string;
        name: string;
        released: string;
        tba: boolean;
        background_image: string;
        rating: number;
        rating_top: number;
        ratings: {
          id: number;
          title: string;
          count: number;
          percent: number;
        }[];
        ratings_count: number;
        reviews_text_count: string;
        added: number;
        added_by_status: {
          yet: number;
          owned: number;
          beaten: number;
          toplay: number;
          dropped: number;
          playing: number;
        };
        metacritic: number;
        playtime: number;
        suggestions_count: number;
        updated: string;
        esrb_rating: {
          id: number;
          slug: string;
          name: string;
        };
        platforms: {
          platform: {
            id: number;
            slug: string;
            name: string;
          };
          released_at: string;
          requirements: {
            minimum: string;
            recommended: string;
          };
        }[];
      }[]
    | null;
}

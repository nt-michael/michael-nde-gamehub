export interface GenreDTO {
  init(res: GenreDTO): void;
  count: number | null;
  next: string | null;
  previous: string | null;
  results:
    | {
        id: number;
        name: string;
        slug: string;
        games_count: number;
        image_background: string;
      }[]
    | null;
}

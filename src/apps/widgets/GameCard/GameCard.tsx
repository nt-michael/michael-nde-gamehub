/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box, Chip, Stack, Typography } from '@mui/material';
// import './game-card.scss'; // Assurez-vous que le chemin vers votre fichier CSS est correct
import { Favorite, Visibility } from '@mui/icons-material';
import { SingleGameDTO } from '../../types/SingleGameDTO';
import { useNavigate } from 'react-router-dom';

const GameCard = ({ data }: { data: SingleGameDTO }) => {
    const navigate = useNavigate();
    function convertToLocal(dateString: any) {
        const date = new Date(dateString);

        const options = {
            year: 'numeric',
            hour12: false
        } as object;

        return date.toLocaleString('en-US', options);
    }

    return (
        <Box margin={1} marginY={4} >
            <Box sx={{ height: "150px" }}>
                <img src={data?.background_image} alt="game card" style={{ height: "-webkit-fill-available", width: "100%" }} className='shadow-md object-cover' />
            </Box>
            <Stack marginTop={2} direction="row" justifyContent="space-between" alignItems="center">
                <img className="w-100" src="/images/logoPlatform.png" alt="platform logo" />
                <Chip label={data.rating} className='p-2' color="success" />
            </Stack>
            <Stack marginTop={1} direction="row" justifyContent="space-between" alignItems="center">
                <Typography variant="h5" onClick={() => navigate(`/app/games/${data.slug}`)} sx={{ color: 'white' }} className='font-bold text-md cursor-pointer'>{data?.name}</Typography>
            </Stack>
            <Stack marginTop={1} direction="row" justifyContent="space-between" alignItems="center">
                <Typography sx={{ color: "gray" }} className='text-sm'>{convertToLocal(data?.updated)}</Typography>
                <Stack direction="row" gap={1}>
                    <Visibility sx={{ color: 'white' }} className='cursor-pointer' />
                    <Favorite sx={{ color: 'white' }} className='cursor-pointer' />
                </Stack>
            </Stack>
        </Box>
    );
};

export default GameCard;

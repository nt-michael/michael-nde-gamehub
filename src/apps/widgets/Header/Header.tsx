import { Box, Chip, Container, Grid, Stack, Typography } from '@mui/material'
import { SingleGameDTO } from '../../types/SingleGameDTO'

const Header = ({ data }: { data: SingleGameDTO }) => {
  return (
    <Box className="w-100" sx={{ height: "100%", width: "100%", background: `linear-gradient(90deg, rgba(0, 0, 0), rgba(0, 0, 0, 0.4) 45.43%, rgba(0, 0, 0, 0) 62.33%), url('${data?.background_image}') no-repeat`, backgroundSize: "cover", backgroundPosition: "center" }} >
      <Container className='p-12' style={{ backdropFilter: 'blur(4px)' }}>
        <Box>
          <Typography variant="h2" className='text-xl font-bold'>{data?.name}</Typography>
        </Box>
        <Stack className="max-w-[500px]">
          <Grid mt={1} direction="row" className='!flex-wrap' container spacing={4}>
            <Grid item xs={6}>
              <Typography variant="h5"
                className="bg-clip-text font-black text-large text-transparent bg-gradient-to-r from-[#DB0041] to-[#2401DB]"
              >
                Genres
              </Typography>
              <Typography>
                {data?.genres && data.genres.map((genre) => (genre.name)).join(', ')}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h5"
                className="bg-clip-text font-black text-large text-transparent bg-gradient-to-r from-[#DB0041] to-[#2401DB]"
              >
                Platforms
              </Typography>
              <Typography>
                {data?.platforms && data.platforms.map((platform) => (platform.platform.name)).join(', ')}
              </Typography>
            </Grid>
          </Grid>

          <Grid mt={1} direction="row" container spacing={4}>
            <Grid item xs={6}>
              <Typography variant="h5"
                className="bg-clip-text font-black text-large text-transparent bg-gradient-to-r from-[#DB0041] to-[#2401DB]"
              >
                Publisher
              </Typography>
              <Typography>
                {data?.publishers && data.publishers.map((publisher) => (publisher.name)).join(', ')}
              </Typography>
            </Grid>
          </Grid>


        </Stack>
        <Stack sx={{ mt: "1rem" }} alignItems="center" gap="1rem" direction="row">
          <Typography>
            Meta Score :
          </Typography>
          <Chip label={data?.metacritic} className='p-2' color="success" />
        </Stack>
      </Container>
    </Box>
  )
}

export default Header
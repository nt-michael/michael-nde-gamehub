import { AppRoute } from "../../configs/type";
import SingleGame from "./SingleGame";

const SingleGameConfig: AppRoute = {
  path: "app/games/:gameid",
  element: <SingleGame />,
};

export default SingleGameConfig;

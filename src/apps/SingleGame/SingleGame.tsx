import Header from '../widgets/Header/Header';
import withLoading from '../../Components/layout/withLoading';
// import './single-game.scss';
import Gallery from './widgets/Gallery/Gallery';
import SimilarGames from './widgets/SimilarGames/SimilarGames';
import { useEffect } from 'react';
import { Container, Grid } from '@mui/material';;
import { useParams } from 'react-router-dom';
import { useSingleGame } from './hooks/useSingleGame';
import { useGameTrailer } from './hooks/useGameTrailer';
import { useGameMedia } from './hooks/useGameMedia';
import { useSingleSimilar } from './hooks/useSingleSimilar';


const SingleGameComp = () => {
    const { gameid } = useParams()
    const { data, refetch } = useSingleGame(gameid as string)
    const { data: trailerContent, refetch: refetchTrailer } = useGameTrailer(gameid as string);
    const { data: mediaContent, refetch: refetchMedia } = useGameMedia(gameid as string);
    const idsArray = data?.genres?.map((item: any) => item.id).join(",") as string;
    const id = data?.id as number
    const { data: similarData, refetch: refecthSimilar } = useSingleSimilar(id, idsArray, 3)



    useEffect(() => {
        refetch()
    }, [gameid])

    useEffect(() => {
        refetchTrailer()
    }, [gameid, data])

    useEffect(() => {
        refetchMedia()
    }, [gameid])
    useEffect(() => {
        refecthSimilar()
    }, [data, gameid])

    return (
        <div>
            <Header data={data as never} />
            <Container className='p-12'>
                <Grid container alignItems="flex-start" justifyContent="center" spacing={4}>
                    <Grid item xs={12} md={8} lg={8} >
                        {data && trailerContent && mediaContent && <Gallery data={data} trailerContent={trailerContent} mediaContent={mediaContent} />}
                    </Grid>
                    <Grid item xs={12} md={4} lg={4} className="sidebar">
                        {similarData && <SimilarGames data={similarData} />}
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

const SingleGame = withLoading(SingleGameComp);
export default SingleGame;
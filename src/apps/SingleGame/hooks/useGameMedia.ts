import { useQuery } from "react-query";
import { getGameMedia } from "../api";

export const useGameMedia = (slug: string) => {
  const queryKey = ["useGameMedia", slug];
  const query = useQuery({
    queryKey,
    queryFn: () => getGameMedia(slug),
    onError: (error: never) => {
      console.log(error);
    },
    retry: false,
    enabled: false,
  });

  return query;
};

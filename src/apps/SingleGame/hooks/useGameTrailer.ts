import { useQuery } from "react-query";
import { getGameTrailer } from "../api";

export const useGameTrailer = (slug: string) => {
  const queryKey = ["useGameTrailer", slug];
  const query = useQuery({
    queryKey,
    queryFn: () => getGameTrailer(slug),
    onError: (error: never) => {
      console.log(error);
    },
    retry: false,
    enabled: false,
  });

  return query;
};

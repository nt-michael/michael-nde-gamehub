import { useQuery } from "react-query";
import { getSingleGame } from "../api";

export const useSingleGame = (slug: string) => {
  const queryKey = ["useSingleGame", slug];
  const query = useQuery({
    queryKey,
    queryFn: () => getSingleGame(slug),
    onError: (error: never) => {
      console.log(error);
    },
    retry: false,
    enabled: false,
  });

  return query;
};

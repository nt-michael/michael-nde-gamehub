import { useQuery } from "react-query";
import { getSimilarGames } from "../api";

export const useSingleSimilar = (
  exclude_collection: number,
  genres: string,
  page_size: number
) => {
  const queryKey = ["useSingleSimilar", exclude_collection, genres, page_size];
  const query = useQuery({
    queryKey,
    queryFn: () => getSimilarGames(exclude_collection, genres, page_size),
    onError: (error: never) => {
      console.log(error);
    },
    retry: false,
    enabled: false,
  });

  return query;
};

import { Box, Grid, Typography } from '@mui/material';
// import './gallery.scss';
// import ReactPlayer from 'react-player';

const Gallery = ({ data, trailerContent, mediaContent }: any) => {

    return (
        <Box>
            <Box>
                <Typography variant="h4" className='font-bold text-md hover:text-red-500 transition-colors duration-300'>
                    Description
                </Typography>
                <Typography sx={{ mt: 2, fontWeight: 300 }} className='text-sm '>
                    {data?.description_raw}
                </Typography>
            </Box>
            <Box sx={{ mt: 6 }}>
                <Typography variant="h4" className='font-bold text-md hover:text-red-500 transition-colors duration-300'>
                    Trailer
                </Typography>
                <Box sx={{ mt: 2 }} >
                    {trailerContent?.results?.length > 0 && <iframe className="w-full h-64" src={trailerContent?.results[0].data.max} frameBorder="0" allowFullScreen></iframe>}

                </Box>
            </Box>
            <Typography sx={{ mt: 6 }} variant="h4" className='font-bold text-md'>
                Gallery
            </Typography>
            <Grid direction="row" container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }} sx={{ mt: 4 }}>
                {mediaContent &&
                    mediaContent.results.map((media: any, idx: number) =>
                        <Grid key={idx} className=' hover:shadow-lg transform hover:scale-105 transition-all duration-300 cursor-pointer' item xs={6}>
                            <img key={idx} src={media.image} alt={"gallery img"} />
                        </Grid>
                    )
                }
            </Grid>

        </Box>
    )
}

export default Gallery
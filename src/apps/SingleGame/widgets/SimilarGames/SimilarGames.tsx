import { Box, Typography } from '@mui/material';
import GameCard from '../../../widgets/GameCard/GameCard';
// import './similar-games.scss';
import { GameDataDTO } from '../../../types';

const SimilarGames = ({ data }: { data: GameDataDTO }) => {
    return (
        <Box sx={{ backgroundColor: "#354250", p: 3, borderRadius: "5px" }}>
            <Typography variant="h4" className='w-100 text-center font-bold text-lg hover:text-red-500 transition-colors duration-300'>Similar Games</Typography>
            <Box sx={{ marginTop: 4 }}>
                {data && data?.results?.map((item) => {
                    return <GameCard key={item?.id} data={item as never} />
                })}
            </Box>
        </Box>
    )
}

export default SimilarGames 
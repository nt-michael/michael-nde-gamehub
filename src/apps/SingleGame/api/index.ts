import axios from "../../../configs/axios";
import { GameDataDTO } from "../../types";
import { ScreenshotDTO } from "../../types/ScreenshotDTO";
import { SingleGameDTO } from "../../types/SingleGameDTO";
import { TrailerMediaDTO } from "../../types/TrailerMediaDTO";

export const getSingleGame = async (slug: string) => {
  const singleGame = await axios.get<SingleGameDTO>(`/games/${slug}`);
  return singleGame?.data;
};

export const getSimilarGames = async (
  exclude_collection: number,
  genres: string,
  page_size: number
) => {
  const similar = await axios.get<GameDataDTO>(
    `/games?exclude_collection=${exclude_collection}&genres=${genres}&page_size=${page_size}`
  );
  return similar.data;
};

export const getGameTrailer = async (slug: string) => {
  const trailer = await axios.get<TrailerMediaDTO>(`/games/${slug}/movies`);
  return trailer?.data;
};

export const getGameMedia = async (slug: string) => {
  const media = await axios.get<ScreenshotDTO>(`/games/${slug}/screenshots`);
  return media?.data;
};

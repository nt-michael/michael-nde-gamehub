import { AppRoute } from "../configs/type";
import ExperimentDevConfig from "./ExperimentDev/ExperimentDevConfig";
import GamesConfig from "./Games/GamesConfig";
import SingleGameConfig from "./SingleGame/SingleGameConfig";

const appConfigs: { routes: AppRoute[] } = {
    routes: [
        GamesConfig,
        SingleGameConfig,
        ExperimentDevConfig
    ]
}

export default appConfigs
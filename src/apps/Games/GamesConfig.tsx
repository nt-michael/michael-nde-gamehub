import Games from "./Games";
import { AppRoute } from "../../configs/type";

const GamesConfig: AppRoute = {
  path: "app/games",
  element: <Games />,
};

export default GamesConfig;

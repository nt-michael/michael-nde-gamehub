import axios from "../../../configs/axios";
import { FilterPayloadDTO } from "../../types";

export const getGames = async (page: number, payload: FilterPayloadDTO) => {
  let requestUrl = `/games?page=${page}`;
  if (payload.search) requestUrl += `&search=${payload.search}`;
  if (payload.genres) requestUrl += `&genres=${payload.genres.join(",")}`;
  if (payload.platforms)
    requestUrl += `&platforms=${payload.platforms.join(",")}`;
  if (payload.ordering) requestUrl += `&ordering=${payload.ordering}`;
  const game = await axios.get(requestUrl);
  return game?.data;
};

export const getGenres = async () => {
  const genres = await axios.get(`/genres`);
  return genres?.data;
};

export const getPlatforms = async () => {
  const platforms = await axios.get(`/platforms/lists/parents`);
  return platforms?.data;
};

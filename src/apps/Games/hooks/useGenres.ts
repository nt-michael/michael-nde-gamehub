import { useQuery } from "react-query";
import { getGenres } from "../api";

export const useGenres = () => {
  const query = useQuery({
    queryKey: ["useGenres"],
    queryFn: async () => await getGenres(),
    onError: (error: never) => {
      console.log(error);
    },
    retry: false,
    enabled: false,
  });

  return query;
};

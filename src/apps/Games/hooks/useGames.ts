import { useQuery } from "react-query";
import { getGames } from "../api";
import { FilterPayloadDTO } from "../../types";

export const useGames = (page: number, payload: FilterPayloadDTO) => {
  const queryKey = ["useGames", page, payload];
  const query = useQuery({
    queryKey,
    queryFn: () => getGames(page, payload),
    onError: (error: never) => {
      console.log(error);
    },
    retry: false,
    enabled: false,
  });

  return query;
};

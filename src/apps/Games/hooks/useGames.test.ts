import { renderHook, act } from "@testing-library/react-hooks";
import { describe, expect, it, vi } from "vitest";
import { useGames } from "./useGames"; // Adjust the path if needed
import { FilterPayloadDTO } from "../../types";

vi.mock("../api"); // Mock the getGames function

describe("useGames hook", () => {
  it("fetches games on initial render", async () => {
    const page = 1;
    const payload: FilterPayloadDTO = {
      search: null,
      ordering: "name",
      genres: null,
      platforms: null,
    };

    const { result, waitFor } = renderHook(() => useGames(page, payload));

    // Assert loading state initially
    expect(result.current.isLoading).toBe(true);

    // Trigger the query execution (usually happens in effect)
    await act(() => {});

    // Wait for the query to resolve
    await waitFor(() => expect(result.current.isLoading).toBe(false));

    // Assert result after fetching
    expect(result.current.data).toBeDefined(); // You might have more specific assertions
  });

  // Add more test cases for different scenarios (error handling, retries, etc.)
});

import { useQuery } from "react-query";
import { getPlatforms } from "../api";

export const usePlatforms = () => {
  const query = useQuery({
    queryKey: ["usePlatforms"],
    queryFn: async () => await getPlatforms(),
    onError: (error: never) => {
      console.log(error);
    },
    retry: false,
    enabled: false,
  });

  return query;
};

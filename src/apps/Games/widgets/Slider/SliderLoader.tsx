import { Container } from "@mui/material";
import React from "react";

const SliderLoader: React.FC = () => {
  return (
    <div className="slider-loader">
      <div className="loader-sub">
        <Container>
          <div className="h1 loading-component"></div>
          <div className="description">
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
          </div>
          <div className="info">
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
          </div>
          <div className="info second">
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
          </div>
          <div className="images">
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
          </div>
          <div className="buttons">
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
          </div>
          <div className="tags">
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
            <div className="sub loading-component"></div>
          </div>
        </Container>
      </div>
    </div>
  );
};

export default SliderLoader;

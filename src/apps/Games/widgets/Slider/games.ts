import { Game } from "../../../../configs/type";

export const games: Game[] = [
  {
    id: 3498,
    slug: "grand-theft-auto-v",
    name: "Grand Theft Auto V",
    released: "2013-09-17",
    tba: false,
    background_image:
      "https://media.rawg.io/media/games/20a/20aa03a10cda45239fe22d035c0ebe64.jpg",
    rating: 4.47,
    rating_top: 5,
    ratings: [
      { id: 5, title: "exceptional", count: 4029, percent: 58.99 },
      { id: 4, title: "recommended", count: 2241, percent: 32.81 },
      { id: 3, title: "meh", count: 435, percent: 6.37 },
      { id: 1, title: "skip", count: 125, percent: 1.83 },
    ],
    ratings_count: 6730,
    reviews_text_count: 58,
    added: 20542,
    added_by_status: {
      yet: 528,
      owned: 11753,
      beaten: 5835,
      toplay: 609,
      dropped: 1095,
      playing: 722,
    },
    metacritic: 92,
    playtime: 74,
    suggestions_count: 429,
    updated: "2024-02-08T18:08:49",
    user_game: null,
    reviews_count: 6830,
    saturated_color: "0f0f0f",
    dominant_color: "0f0f0f",
    platforms: [
      {
        platform: {
          id: 4,
          name: "PC",
          slug: "pc",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 525051,
          image_background:
            "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
        },
        released_at: "2013-09-17",
        requirements_en: {
          minimum:
            "Minimum:OS: Windows 10 64 Bit, Windows 8.1 64 Bit, Windows 8 64 Bit, Windows 7 64 Bit Service Pack 1, Windows Vista 64 Bit Service Pack 2* (*NVIDIA video card recommended if running Vista OS)Processor: Intel Core 2 Quad CPU Q6600 @ 2.40GHz (4 CPUs) / AMD Phenom 9850 Quad-Core Processor (4 CPUs) @ 2.5GHzMemory: 4 GB RAMGraphics: NVIDIA 9800 GT 1GB / AMD HD 4870 1GB (DX 10, 10.1, 11)Storage: 72 GB available spaceSound Card: 100% DirectX 10 compatibleAdditional Notes: Over time downloadable content and programming changes will change the system requirements for this game.  Please refer to your hardware manufacturer and www.rockstargames.com/support for current compatibility information. Some system components such as mobile chipsets, integrated, and AGP graphics cards may be incompatible. Unlisted specifications may not be supported by publisher.     Other requirements:  Installation and online play requires log-in to Rockstar Games Social Club (13+) network; internet connection required for activation, online play, and periodic entitlement verification; software installations required including Rockstar Games Social Club platform, DirectX , Chromium, and Microsoft Visual C++ 2008 sp1 Redistributable Package, and authentication software that recognizes certain hardware attributes for entitlement, digital rights management, system, and other support purposes.     SINGLE USE SERIAL CODE REGISTRATION VIA INTERNET REQUIRED; REGISTRATION IS LIMITED TO ONE ROCKSTAR GAMES SOCIAL CLUB ACCOUNT (13+) PER SERIAL CODE; ONLY ONE PC LOG-IN ALLOWED PER SOCIAL CLUB ACCOUNT AT ANY TIME; SERIAL CODE(S) ARE NON-TRANSFERABLE ONCE USED; SOCIAL CLUB ACCOUNTS ARE NON-TRANSFERABLE.  Partner Requirements:  Please check the terms of service of this site before purchasing this software.",
          recommended:
            "Recommended:OS: Windows 10 64 Bit, Windows 8.1 64 Bit, Windows 8 64 Bit, Windows 7 64 Bit Service Pack 1Processor: Intel Core i5 3470 @ 3.2GHz (4 CPUs) / AMD X8 FX-8350 @ 4GHz (8 CPUs)Memory: 8 GB RAMGraphics: NVIDIA GTX 660 2GB / AMD HD 7870 2GBStorage: 72 GB available spaceSound Card: 100% DirectX 10 compatibleAdditional Notes:",
        },
        requirements_ru: null,
      },
      {
        platform: {
          id: 187,
          name: "PlayStation 5",
          slug: "playstation5",
          image: null,
          year_end: null,
          year_start: 2020,
          games_count: 1012,
          image_background:
            "https://media.rawg.io/media/games/709/709bf81f874ce5d25d625b37b014cb63.jpg",
        },
        released_at: "2013-09-17",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 186,
          name: "Xbox Series S/X",
          slug: "xbox-series-x",
          image: null,
          year_end: null,
          year_start: 2020,
          games_count: 877,
          image_background:
            "https://media.rawg.io/media/games/d47/d479582ed0a46496ad34f65c7099d7e5.jpg",
        },
        released_at: "2013-09-17",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 18,
          name: "PlayStation 4",
          slug: "playstation4",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 6761,
          image_background:
            "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
        },
        released_at: "2013-09-17",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 16,
          name: "PlayStation 3",
          slug: "playstation3",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 3168,
          image_background:
            "https://media.rawg.io/media/games/20a/20aa03a10cda45239fe22d035c0ebe64.jpg",
        },
        released_at: "2013-09-17",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 14,
          name: "Xbox 360",
          slug: "xbox360",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 2796,
          image_background:
            "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg",
        },
        released_at: "2013-09-17",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 1,
          name: "Xbox One",
          slug: "xbox-one",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 5585,
          image_background:
            "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg",
        },
        released_at: "2013-09-17",
        requirements_en: null,
        requirements_ru: null,
      },
    ],
    parent_platforms: [
      { platform: { id: 1, name: "PC", slug: "pc" } },
      { platform: { id: 2, name: "PlayStation", slug: "playstation" } },
      { platform: { id: 3, name: "Xbox", slug: "xbox" } },
    ],
    genres: [
      {
        id: 4,
        name: "Action",
        slug: "action",
        games_count: 178012,
        image_background:
          "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg",
      },
    ],
    stores: [
      {
        id: 290375,
        store: {
          id: 3,
          name: "PlayStation Store",
          slug: "playstation-store",
          domain: "store.playstation.com",
          games_count: 7894,
          image_background:
            "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg",
        },
      },
      {
        id: 438095,
        store: {
          id: 11,
          name: "Epic Games",
          slug: "epic-games",
          domain: "epicgames.com",
          games_count: 1318,
          image_background:
            "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg",
        },
      },
      {
        id: 290376,
        store: {
          id: 1,
          name: "Steam",
          slug: "steam",
          domain: "store.steampowered.com",
          games_count: 88246,
          image_background:
            "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg",
        },
      },
      {
        id: 290377,
        store: {
          id: 7,
          name: "Xbox 360 Store",
          slug: "xbox360",
          domain: "marketplace.xbox.com",
          games_count: 1912,
          image_background:
            "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg",
        },
      },
      {
        id: 290378,
        store: {
          id: 2,
          name: "Xbox Store",
          slug: "xbox-store",
          domain: "microsoft.com",
          games_count: 4806,
          image_background:
            "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg",
        },
      },
    ],
    clip: null,
    tags: [
      {
        id: 31,
        name: "Singleplayer",
        slug: "singleplayer",
        language: "eng",
        games_count: 217412,
        image_background:
          "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg",
      },
      {
        id: 40847,
        name: "Steam Achievements",
        slug: "steam-achievements",
        language: "eng",
        games_count: 35394,
        image_background:
          "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg",
      },
      {
        id: 7,
        name: "Multiplayer",
        slug: "multiplayer",
        language: "eng",
        games_count: 37026,
        image_background:
          "https://media.rawg.io/media/games/ec3/ec3a7db7b8ab5a71aad622fe7c62632f.jpg",
      },
      {
        id: 40836,
        name: "Full controller support",
        slug: "full-controller-support",
        language: "eng",
        games_count: 16589,
        image_background:
          "https://media.rawg.io/media/games/2ba/2bac0e87cf45e5b508f227d281c9252a.jpg",
      },
      {
        id: 13,
        name: "Atmospheric",
        slug: "atmospheric",
        language: "eng",
        games_count: 31870,
        image_background:
          "https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg",
      },
      {
        id: 42,
        name: "Great Soundtrack",
        slug: "great-soundtrack",
        language: "eng",
        games_count: 3385,
        image_background:
          "https://media.rawg.io/media/games/4cf/4cfc6b7f1850590a4634b08bfab308ab.jpg",
      },
      {
        id: 24,
        name: "RPG",
        slug: "rpg",
        language: "eng",
        games_count: 19685,
        image_background:
          "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg",
      },
      {
        id: 18,
        name: "Co-op",
        slug: "co-op",
        language: "eng",
        games_count: 11002,
        image_background:
          "https://media.rawg.io/media/games/55e/55ee6432ac2bf224610fa17e4c652107.jpg",
      },
      {
        id: 36,
        name: "Open World",
        slug: "open-world",
        language: "eng",
        games_count: 7064,
        image_background:
          "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
      },
      {
        id: 411,
        name: "cooperative",
        slug: "cooperative",
        language: "eng",
        games_count: 4663,
        image_background:
          "https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg",
      },
      {
        id: 8,
        name: "First-Person",
        slug: "first-person",
        language: "eng",
        games_count: 30485,
        image_background:
          "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg",
      },
      {
        id: 149,
        name: "Third Person",
        slug: "third-person",
        language: "eng",
        games_count: 10777,
        image_background:
          "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
      },
      {
        id: 4,
        name: "Funny",
        slug: "funny",
        language: "eng",
        games_count: 24051,
        image_background:
          "https://media.rawg.io/media/games/af7/af7a831001c5c32c46e950cc883b8cb7.jpg",
      },
      {
        id: 37,
        name: "Sandbox",
        slug: "sandbox",
        language: "eng",
        games_count: 6565,
        image_background:
          "https://media.rawg.io/media/games/d1a/d1a2e99ade53494c6330a0ed945fe823.jpg",
      },
      {
        id: 123,
        name: "Comedy",
        slug: "comedy",
        language: "eng",
        games_count: 11891,
        image_background:
          "https://media.rawg.io/media/games/85c/85c8ae70e7cdf0105f06ef6bdce63b8b.jpg",
      },
      {
        id: 150,
        name: "Third-Person Shooter",
        slug: "third-person-shooter",
        language: "eng",
        games_count: 3246,
        image_background:
          "https://media.rawg.io/media/games/974/974342a3959981a17bdbbff2fd7f97b0.jpg",
      },
      {
        id: 62,
        name: "Moddable",
        slug: "moddable",
        language: "eng",
        games_count: 873,
        image_background:
          "https://media.rawg.io/media/games/476/476178ef18ab0534771d099f51cdc694.jpg",
      },
      {
        id: 144,
        name: "Crime",
        slug: "crime",
        language: "eng",
        games_count: 2710,
        image_background:
          "https://media.rawg.io/media/games/3d9/3d9bac98d79bcd2d445f829e8d6be902.jpg",
      },
      {
        id: 62349,
        name: "vr mod",
        slug: "vr-mod",
        language: "eng",
        games_count: 17,
        image_background:
          "https://media.rawg.io/media/screenshots/1bb/1bb3f78f0fe43b5d5ca2f3da5b638840.jpg",
      },
    ],
    esrb_rating: { id: 4, name: "Mature", slug: "mature" },
    short_screenshots: [
      {
        id: -1,
        image:
          "https://media.rawg.io/media/games/20a/20aa03a10cda45239fe22d035c0ebe64.jpg",
      },
      {
        id: 1827221,
        image:
          "https://media.rawg.io/media/screenshots/a7c/a7c43871a54bed6573a6a429451564ef.jpg",
      },
      {
        id: 1827222,
        image:
          "https://media.rawg.io/media/screenshots/cf4/cf4367daf6a1e33684bf19adb02d16d6.jpg",
      },
      {
        id: 1827223,
        image:
          "https://media.rawg.io/media/screenshots/f95/f9518b1d99210c0cae21fc09e95b4e31.jpg",
      },
      {
        id: 1827225,
        image:
          "https://media.rawg.io/media/screenshots/a5c/a5c95ea539c87d5f538763e16e18fb99.jpg",
      },
      {
        id: 1827226,
        image:
          "https://media.rawg.io/media/screenshots/a7e/a7e990bc574f4d34e03b5926361d1ee7.jpg",
      },
      {
        id: 1827227,
        image:
          "https://media.rawg.io/media/screenshots/592/592e2501d8734b802b2a34fee2df59fa.jpg",
      },
    ],
  },
  {
    id: 3328,
    slug: "the-witcher-3-wild-hunt",
    name: "The Witcher 3: Wild Hunt",
    released: "2015-05-18",
    tba: false,
    background_image:
      "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg",
    rating: 4.65,
    rating_top: 5,
    ratings: [
      { id: 5, title: "exceptional", count: 5051, percent: 77.15 },
      { id: 4, title: "recommended", count: 1053, percent: 16.08 },
      { id: 3, title: "meh", count: 278, percent: 4.25 },
      { id: 1, title: "skip", count: 165, percent: 2.52 },
    ],
    ratings_count: 6444,
    reviews_text_count: 71,
    added: 19820,
    added_by_status: {
      yet: 1109,
      owned: 11398,
      beaten: 4746,
      toplay: 775,
      dropped: 929,
      playing: 863,
    },
    metacritic: 92,
    playtime: 46,
    suggestions_count: 675,
    updated: "2024-02-09T17:51:59",
    user_game: null,
    reviews_count: 6547,
    saturated_color: "0f0f0f",
    dominant_color: "0f0f0f",
    platforms: [
      {
        platform: {
          id: 186,
          name: "Xbox Series S/X",
          slug: "xbox-series-x",
          image: null,
          year_end: null,
          year_start: 2020,
          games_count: 877,
          image_background:
            "https://media.rawg.io/media/games/d47/d479582ed0a46496ad34f65c7099d7e5.jpg",
        },
        released_at: "2015-05-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 187,
          name: "PlayStation 5",
          slug: "playstation5",
          image: null,
          year_end: null,
          year_start: 2020,
          games_count: 1012,
          image_background:
            "https://media.rawg.io/media/games/709/709bf81f874ce5d25d625b37b014cb63.jpg",
        },
        released_at: "2015-05-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 5,
          name: "macOS",
          slug: "macos",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 103290,
          image_background:
            "https://media.rawg.io/media/games/ee3/ee3e10193aafc3230ba1cae426967d10.jpg",
        },
        released_at: "2015-05-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 18,
          name: "PlayStation 4",
          slug: "playstation4",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 6761,
          image_background:
            "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
        },
        released_at: "2015-05-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 7,
          name: "Nintendo Switch",
          slug: "nintendo-switch",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 5401,
          image_background:
            "https://media.rawg.io/media/games/b4e/b4e4c73d5aa4ec66bbf75375c4847a2b.jpg",
        },
        released_at: "2015-05-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 4,
          name: "PC",
          slug: "pc",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 525051,
          image_background:
            "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
        },
        released_at: "2015-05-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 1,
          name: "Xbox One",
          slug: "xbox-one",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 5585,
          image_background:
            "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg",
        },
        released_at: "2015-05-18",
        requirements_en: null,
        requirements_ru: null,
      },
    ],
    parent_platforms: [
      { platform: { id: 1, name: "PC", slug: "pc" } },
      { platform: { id: 2, name: "PlayStation", slug: "playstation" } },
      { platform: { id: 3, name: "Xbox", slug: "xbox" } },
      { platform: { id: 5, name: "Apple Macintosh", slug: "mac" } },
      { platform: { id: 7, name: "Nintendo", slug: "nintendo" } },
    ],
    genres: [
      {
        id: 4,
        name: "Action",
        slug: "action",
        games_count: 178012,
        image_background:
          "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg",
      },
      {
        id: 5,
        name: "RPG",
        slug: "role-playing-games-rpg",
        games_count: 54930,
        image_background:
          "https://media.rawg.io/media/games/1f4/1f47a270b8f241e4676b14d39ec620f7.jpg",
      },
    ],
    stores: [
      {
        id: 354780,
        store: {
          id: 5,
          name: "GOG",
          slug: "gog",
          domain: "gog.com",
          games_count: 5775,
          image_background:
            "https://media.rawg.io/media/games/c80/c80bcf321da44d69b18a06c04d942662.jpg",
        },
      },
      {
        id: 3565,
        store: {
          id: 3,
          name: "PlayStation Store",
          slug: "playstation-store",
          domain: "store.playstation.com",
          games_count: 7894,
          image_background:
            "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg",
        },
      },
      {
        id: 305376,
        store: {
          id: 1,
          name: "Steam",
          slug: "steam",
          domain: "store.steampowered.com",
          games_count: 88246,
          image_background:
            "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg",
        },
      },
      {
        id: 312313,
        store: {
          id: 2,
          name: "Xbox Store",
          slug: "xbox-store",
          domain: "microsoft.com",
          games_count: 4806,
          image_background:
            "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg",
        },
      },
      {
        id: 676022,
        store: {
          id: 6,
          name: "Nintendo Store",
          slug: "nintendo",
          domain: "nintendo.com",
          games_count: 8961,
          image_background:
            "https://media.rawg.io/media/games/4fb/4fb548e4816c84d1d70f1a228fb167cc.jpg",
        },
      },
    ],
    clip: null,
    tags: [
      {
        id: 31,
        name: "Singleplayer",
        slug: "singleplayer",
        language: "eng",
        games_count: 217412,
        image_background:
          "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg",
      },
      {
        id: 40836,
        name: "Full controller support",
        slug: "full-controller-support",
        language: "eng",
        games_count: 16589,
        image_background:
          "https://media.rawg.io/media/games/2ba/2bac0e87cf45e5b508f227d281c9252a.jpg",
      },
      {
        id: 13,
        name: "Atmospheric",
        slug: "atmospheric",
        language: "eng",
        games_count: 31870,
        image_background:
          "https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg",
      },
      {
        id: 42,
        name: "Great Soundtrack",
        slug: "great-soundtrack",
        language: "eng",
        games_count: 3385,
        image_background:
          "https://media.rawg.io/media/games/4cf/4cfc6b7f1850590a4634b08bfab308ab.jpg",
      },
      {
        id: 24,
        name: "RPG",
        slug: "rpg",
        language: "eng",
        games_count: 19685,
        image_background:
          "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg",
      },
      {
        id: 118,
        name: "Story Rich",
        slug: "story-rich",
        language: "eng",
        games_count: 20391,
        image_background:
          "https://media.rawg.io/media/games/310/3106b0e012271c5ffb16497b070be739.jpg",
      },
      {
        id: 36,
        name: "Open World",
        slug: "open-world",
        language: "eng",
        games_count: 7064,
        image_background:
          "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
      },
      {
        id: 149,
        name: "Third Person",
        slug: "third-person",
        language: "eng",
        games_count: 10777,
        image_background:
          "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
      },
      {
        id: 64,
        name: "Fantasy",
        slug: "fantasy",
        language: "eng",
        games_count: 26613,
        image_background:
          "https://media.rawg.io/media/games/b6b/b6b20bfc4b34e312dbc8aac53c95a348.jpg",
      },
      {
        id: 37,
        name: "Sandbox",
        slug: "sandbox",
        language: "eng",
        games_count: 6565,
        image_background:
          "https://media.rawg.io/media/games/d1a/d1a2e99ade53494c6330a0ed945fe823.jpg",
      },
      {
        id: 97,
        name: "Action RPG",
        slug: "action-rpg",
        language: "eng",
        games_count: 6447,
        image_background:
          "https://media.rawg.io/media/games/af7/af7a831001c5c32c46e950cc883b8cb7.jpg",
      },
      {
        id: 41,
        name: "Dark",
        slug: "dark",
        language: "eng",
        games_count: 15116,
        image_background:
          "https://media.rawg.io/media/games/157/15742f2f67eacff546738e1ab5c19d20.jpg",
      },
      {
        id: 44,
        name: "Nudity",
        slug: "nudity",
        language: "eng",
        games_count: 5996,
        image_background:
          "https://media.rawg.io/media/games/260/26023c855f1769a93411d6a7ea084632.jpeg",
      },
      {
        id: 336,
        name: "controller support",
        slug: "controller-support",
        language: "eng",
        games_count: 293,
        image_background:
          "https://media.rawg.io/media/games/942/9424d6bb763dc38d9378b488603c87fa.jpg",
      },
      {
        id: 145,
        name: "Choices Matter",
        slug: "choices-matter",
        language: "eng",
        games_count: 4720,
        image_background:
          "https://media.rawg.io/media/games/07a/07a74470a2618fd71945db0619602baf.jpg",
      },
      {
        id: 192,
        name: "Mature",
        slug: "mature",
        language: "eng",
        games_count: 2824,
        image_background:
          "https://media.rawg.io/media/games/849/849414b978db37d4563ff9e4b0d3a787.jpg",
      },
      {
        id: 40,
        name: "Dark Fantasy",
        slug: "dark-fantasy",
        language: "eng",
        games_count: 3840,
        image_background:
          "https://media.rawg.io/media/games/a6c/a6ccd34125c594abf1a9c9821b9a715d.jpg",
      },
      {
        id: 66,
        name: "Medieval",
        slug: "medieval",
        language: "eng",
        games_count: 6037,
        image_background:
          "https://media.rawg.io/media/games/155/155a7d8f464ef6029e11cc6a9c0f763d.jpg",
      },
      {
        id: 82,
        name: "Magic",
        slug: "magic",
        language: "eng",
        games_count: 8954,
        image_background:
          "https://media.rawg.io/media/games/c7a/c7a71a0531a9518236d99d0d60abe447.jpg",
      },
      {
        id: 218,
        name: "Multiple Endings",
        slug: "multiple-endings",
        language: "eng",
        games_count: 8133,
        image_background:
          "https://media.rawg.io/media/games/6cd/6cd653e0aaef5ff8bbd295bf4bcb12eb.jpg",
      },
    ],
    esrb_rating: { id: 4, name: "Mature", slug: "mature" },
    short_screenshots: [
      {
        id: -1,
        image:
          "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg",
      },
      {
        id: 30336,
        image:
          "https://media.rawg.io/media/screenshots/1ac/1ac19f31974314855ad7be266adeb500.jpg",
      },
      {
        id: 30337,
        image:
          "https://media.rawg.io/media/screenshots/6a0/6a08afca95261a2fe221ea9e01d28762.jpg",
      },
      {
        id: 30338,
        image:
          "https://media.rawg.io/media/screenshots/cdd/cdd31b6b4a687425a87b5ce231ac89d7.jpg",
      },
      {
        id: 30339,
        image:
          "https://media.rawg.io/media/screenshots/862/862397b153221a625922d3bb66337834.jpg",
      },
      {
        id: 30340,
        image:
          "https://media.rawg.io/media/screenshots/166/166787c442a45f52f4f230c33fd7d605.jpg",
      },
      {
        id: 30342,
        image:
          "https://media.rawg.io/media/screenshots/f63/f6373ee614046d81503d63f167181803.jpg",
      },
    ],
  },
  {
    id: 4200,
    slug: "portal-2",
    name: "Portal 2",
    released: "2011-04-18",
    tba: false,
    background_image:
      "https://media.rawg.io/media/games/2ba/2bac0e87cf45e5b508f227d281c9252a.jpg",
    rating: 4.61,
    rating_top: 5,
    ratings: [
      { id: 5, title: "exceptional", count: 3956, percent: 70.23 },
      { id: 4, title: "recommended", count: 1401, percent: 24.87 },
      { id: 3, title: "meh", count: 152, percent: 2.7 },
      { id: 1, title: "skip", count: 124, percent: 2.2 },
    ],
    ratings_count: 5581,
    reviews_text_count: 34,
    added: 18715,
    added_by_status: {
      yet: 615,
      owned: 11553,
      beaten: 5444,
      toplay: 376,
      dropped: 579,
      playing: 148,
    },
    metacritic: 95,
    playtime: 11,
    suggestions_count: 548,
    updated: "2024-02-07T18:58:23",
    user_game: null,
    reviews_count: 5633,
    saturated_color: "0f0f0f",
    dominant_color: "0f0f0f",
    platforms: [
      {
        platform: {
          id: 16,
          name: "PlayStation 3",
          slug: "playstation3",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 3168,
          image_background:
            "https://media.rawg.io/media/games/20a/20aa03a10cda45239fe22d035c0ebe64.jpg",
        },
        released_at: "2011-04-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 4,
          name: "PC",
          slug: "pc",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 525051,
          image_background:
            "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
        },
        released_at: "2011-04-18",
        requirements_en: null,
        requirements_ru: {
          minimum:
            "Core 2 Duo/Athlon X2 2 ГГц,1 Гб памяти,GeForce 7600/Radeon X800,10 Гб на винчестере,интернет-соединение",
          recommended:
            "Core 2 Duo/Athlon X2 2.5 ГГц,2 Гб памяти,GeForce GTX 280/Radeon HD 2600,10 Гб на винчестере,интернет-соединение",
        },
      },
      {
        platform: {
          id: 14,
          name: "Xbox 360",
          slug: "xbox360",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 2796,
          image_background:
            "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg",
        },
        released_at: "2011-04-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 6,
          name: "Linux",
          slug: "linux",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 76375,
          image_background:
            "https://media.rawg.io/media/games/d1a/d1a2e99ade53494c6330a0ed945fe823.jpg",
        },
        released_at: "2011-04-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 5,
          name: "macOS",
          slug: "macos",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 103290,
          image_background:
            "https://media.rawg.io/media/games/ee3/ee3e10193aafc3230ba1cae426967d10.jpg",
        },
        released_at: "2011-04-18",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 1,
          name: "Xbox One",
          slug: "xbox-one",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 5585,
          image_background:
            "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg",
        },
        released_at: "2011-04-18",
        requirements_en: null,
        requirements_ru: null,
      },
    ],
    parent_platforms: [
      { platform: { id: 1, name: "PC", slug: "pc" } },
      { platform: { id: 2, name: "PlayStation", slug: "playstation" } },
      { platform: { id: 3, name: "Xbox", slug: "xbox" } },
      { platform: { id: 5, name: "Apple Macintosh", slug: "mac" } },
      { platform: { id: 6, name: "Linux", slug: "linux" } },
    ],
    genres: [
      {
        id: 2,
        name: "Shooter",
        slug: "shooter",
        games_count: 59449,
        image_background:
          "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg",
      },
      {
        id: 7,
        name: "Puzzle",
        slug: "puzzle",
        games_count: 97215,
        image_background:
          "https://media.rawg.io/media/games/3ef/3eff92562640e452d3487c04ba6d7fae.jpg",
      },
    ],
    stores: [
      {
        id: 465889,
        store: {
          id: 2,
          name: "Xbox Store",
          slug: "xbox-store",
          domain: "microsoft.com",
          games_count: 4806,
          image_background:
            "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg",
        },
      },
      {
        id: 13134,
        store: {
          id: 1,
          name: "Steam",
          slug: "steam",
          domain: "store.steampowered.com",
          games_count: 88246,
          image_background:
            "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg",
        },
      },
      {
        id: 4526,
        store: {
          id: 3,
          name: "PlayStation Store",
          slug: "playstation-store",
          domain: "store.playstation.com",
          games_count: 7894,
          image_background:
            "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg",
        },
      },
      {
        id: 33916,
        store: {
          id: 7,
          name: "Xbox 360 Store",
          slug: "xbox360",
          domain: "marketplace.xbox.com",
          games_count: 1912,
          image_background:
            "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg",
        },
      },
    ],
    clip: null,
    tags: [
      {
        id: 31,
        name: "Singleplayer",
        slug: "singleplayer",
        language: "eng",
        games_count: 217412,
        image_background:
          "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg",
      },
      {
        id: 40847,
        name: "Steam Achievements",
        slug: "steam-achievements",
        language: "eng",
        games_count: 35394,
        image_background:
          "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg",
      },
      {
        id: 7,
        name: "Multiplayer",
        slug: "multiplayer",
        language: "eng",
        games_count: 37026,
        image_background:
          "https://media.rawg.io/media/games/ec3/ec3a7db7b8ab5a71aad622fe7c62632f.jpg",
      },
      {
        id: 40836,
        name: "Full controller support",
        slug: "full-controller-support",
        language: "eng",
        games_count: 16589,
        image_background:
          "https://media.rawg.io/media/games/2ba/2bac0e87cf45e5b508f227d281c9252a.jpg",
      },
      {
        id: 40849,
        name: "Steam Cloud",
        slug: "steam-cloud",
        language: "eng",
        games_count: 16623,
        image_background:
          "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg",
      },
      {
        id: 13,
        name: "Atmospheric",
        slug: "atmospheric",
        language: "eng",
        games_count: 31870,
        image_background:
          "https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg",
      },
      {
        id: 7808,
        name: "steam-trading-cards",
        slug: "steam-trading-cards",
        language: "eng",
        games_count: 7569,
        image_background:
          "https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg",
      },
      {
        id: 18,
        name: "Co-op",
        slug: "co-op",
        language: "eng",
        games_count: 11002,
        image_background:
          "https://media.rawg.io/media/games/55e/55ee6432ac2bf224610fa17e4c652107.jpg",
      },
      {
        id: 118,
        name: "Story Rich",
        slug: "story-rich",
        language: "eng",
        games_count: 20391,
        image_background:
          "https://media.rawg.io/media/games/310/3106b0e012271c5ffb16497b070be739.jpg",
      },
      {
        id: 411,
        name: "cooperative",
        slug: "cooperative",
        language: "eng",
        games_count: 4663,
        image_background:
          "https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg",
      },
      {
        id: 8,
        name: "First-Person",
        slug: "first-person",
        language: "eng",
        games_count: 30485,
        image_background:
          "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg",
      },
      {
        id: 32,
        name: "Sci-fi",
        slug: "sci-fi",
        language: "eng",
        games_count: 18439,
        image_background:
          "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg",
      },
      {
        id: 30,
        name: "FPS",
        slug: "fps",
        language: "eng",
        games_count: 12837,
        image_background:
          "https://media.rawg.io/media/games/49c/49c3dfa4ce2f6f140cc4825868e858cb.jpg",
      },
      {
        id: 9,
        name: "Online Co-Op",
        slug: "online-co-op",
        language: "eng",
        games_count: 5158,
        image_background:
          "https://media.rawg.io/media/games/21c/21cc15d233117c6809ec86870559e105.jpg",
      },
      {
        id: 4,
        name: "Funny",
        slug: "funny",
        language: "eng",
        games_count: 24051,
        image_background:
          "https://media.rawg.io/media/games/af7/af7a831001c5c32c46e950cc883b8cb7.jpg",
      },
      {
        id: 189,
        name: "Female Protagonist",
        slug: "female-protagonist",
        language: "eng",
        games_count: 11736,
        image_background:
          "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg",
      },
      {
        id: 123,
        name: "Comedy",
        slug: "comedy",
        language: "eng",
        games_count: 11891,
        image_background:
          "https://media.rawg.io/media/games/85c/85c8ae70e7cdf0105f06ef6bdce63b8b.jpg",
      },
      {
        id: 75,
        name: "Local Co-Op",
        slug: "local-co-op",
        language: "eng",
        games_count: 5272,
        image_background:
          "https://media.rawg.io/media/games/9f1/9f1891779cb20f44de93cef33b067e50.jpg",
      },
      {
        id: 11669,
        name: "stats",
        slug: "stats",
        language: "eng",
        games_count: 4799,
        image_background:
          "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg",
      },
      {
        id: 40852,
        name: "Steam Workshop",
        slug: "steam-workshop",
        language: "eng",
        games_count: 1434,
        image_background:
          "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg",
      },
      {
        id: 25,
        name: "Space",
        slug: "space",
        language: "eng",
        games_count: 42456,
        image_background:
          "https://media.rawg.io/media/games/ac7/ac7b8327343da12c971cfc418f390a11.jpg",
      },
      {
        id: 40838,
        name: "Includes level editor",
        slug: "includes-level-editor",
        language: "eng",
        games_count: 1805,
        image_background:
          "https://media.rawg.io/media/games/48c/48cb04ca483be865e3a83119c94e6097.jpg",
      },
      {
        id: 40833,
        name: "Captions available",
        slug: "captions-available",
        language: "eng",
        games_count: 1305,
        image_background:
          "https://media.rawg.io/media/games/9e5/9e5b91a6d02e66b8d450a977a59ae123.jpg",
      },
      {
        id: 40834,
        name: "Commentary available",
        slug: "commentary-available",
        language: "eng",
        games_count: 266,
        image_background:
          "https://media.rawg.io/media/games/594/5949baae74fe9e399adbce0c44e28783.jpg",
      },
      {
        id: 87,
        name: "Science",
        slug: "science",
        language: "eng",
        games_count: 1650,
        image_background:
          "https://media.rawg.io/media/screenshots/fcc/fccf1a1d0c1615d4132bc779b4de7bb6.jpg",
      },
    ],
    esrb_rating: { id: 2, name: "Everyone 10+", slug: "everyone-10-plus" },
    short_screenshots: [
      {
        id: -1,
        image:
          "https://media.rawg.io/media/games/2ba/2bac0e87cf45e5b508f227d281c9252a.jpg",
      },
      {
        id: 99018,
        image:
          "https://media.rawg.io/media/screenshots/221/221a03c11e5ff9f765d62f60d4b4cbf5.jpg",
      },
      {
        id: 99019,
        image:
          "https://media.rawg.io/media/screenshots/173/1737ff43c14f40294011a209b1012875.jpg",
      },
      {
        id: 99020,
        image:
          "https://media.rawg.io/media/screenshots/b11/b11a2ae0664f0e8a1ef2346f99df26e1.jpg",
      },
      {
        id: 99021,
        image:
          "https://media.rawg.io/media/screenshots/9b1/9b107a790909b31918ebe2f40547cc85.jpg",
      },
      {
        id: 99022,
        image:
          "https://media.rawg.io/media/screenshots/d05/d058fc7f7fa6128916c311eb14267fed.jpg",
      },
      {
        id: 99023,
        image:
          "https://media.rawg.io/media/screenshots/415/41543dcc12dffc8e97d85a56ad42cda8.jpg",
      },
    ],
  },
  {
    id: 4291,
    slug: "counter-strike-global-offensive",
    name: "Counter-Strike: Global Offensive",
    released: "2012-08-21",
    tba: false,
    background_image:
      "https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg",
    rating: 3.57,
    rating_top: 4,
    ratings: [
      { id: 4, title: "recommended", count: 1619, percent: 46.74 },
      { id: 3, title: "meh", count: 901, percent: 26.01 },
      { id: 5, title: "exceptional", count: 558, percent: 16.11 },
      { id: 1, title: "skip", count: 386, percent: 11.14 },
    ],
    ratings_count: 3429,
    reviews_text_count: 26,
    added: 16313,
    added_by_status: {
      yet: 261,
      owned: 12345,
      beaten: 1017,
      toplay: 77,
      dropped: 1984,
      playing: 629,
    },
    metacritic: 81,
    playtime: 65,
    suggestions_count: 584,
    updated: "2024-02-05T05:09:17",
    user_game: null,
    reviews_count: 3464,
    saturated_color: "0f0f0f",
    dominant_color: "0f0f0f",
    platforms: [
      {
        platform: {
          id: 4,
          name: "PC",
          slug: "pc",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 525051,
          image_background:
            "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
        },
        released_at: "2012-08-21",
        requirements_en: {
          minimum:
            '<strong>Minimum:</strong><br><ul class="bb_ul"><li><strong>OS:</strong> Windows® 7/Vista/XP<br></li><li><strong>Processor:</strong> Intel® Core™ 2 Duo E6600 or AMD Phenom™ X3 8750 processor or better<br></li><li><strong>Memory:</strong> 2 GB RAM<br></li><li><strong>Graphics:</strong> Video card must be 256 MB or more and should be a DirectX 9-compatible with support for Pixel Shader 3.0<br></li><li><strong>DirectX:</strong> Version 9.0c<br></li><li><strong>Storage:</strong> 15 GB available space</li></ul>',
        },
        requirements_ru: {
          minimum:
            "Сore 2 Duo/Athlon x2 64 1.8 ГГц,2 Гб памяти,GeForce 8800/Radeon X9800,7.6 Гб на винчестере,интернет-соединение",
          recommended:
            "Core 2 Duo E6600/Phenom X3 8750,4 Гб памяти,GeForce 480 GTX/Radeon HD 6970,7.6 Гб на винчестере,интернет-соединение",
        },
      },
      {
        platform: {
          id: 14,
          name: "Xbox 360",
          slug: "xbox360",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 2796,
          image_background:
            "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg",
        },
        released_at: "2012-08-21",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 16,
          name: "PlayStation 3",
          slug: "playstation3",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 3168,
          image_background:
            "https://media.rawg.io/media/games/20a/20aa03a10cda45239fe22d035c0ebe64.jpg",
        },
        released_at: "2012-08-21",
        requirements_en: null,
        requirements_ru: null,
      },
    ],
    parent_platforms: [
      { platform: { id: 1, name: "PC", slug: "pc" } },
      { platform: { id: 2, name: "PlayStation", slug: "playstation" } },
      { platform: { id: 3, name: "Xbox", slug: "xbox" } },
    ],
    genres: [
      {
        id: 2,
        name: "Shooter",
        slug: "shooter",
        games_count: 59449,
        image_background:
          "https://media.rawg.io/media/games/15c/15c95a4915f88a3e89c821526afe05fc.jpg",
      },
    ],
    stores: [
      {
        id: 4619,
        store: {
          id: 3,
          name: "PlayStation Store",
          slug: "playstation-store",
          domain: "store.playstation.com",
          games_count: 7894,
          image_background:
            "https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg",
        },
      },
      {
        id: 11489,
        store: {
          id: 1,
          name: "Steam",
          slug: "steam",
          domain: "store.steampowered.com",
          games_count: 88246,
          image_background:
            "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg",
        },
      },
      {
        id: 49169,
        store: {
          id: 7,
          name: "Xbox 360 Store",
          slug: "xbox360",
          domain: "marketplace.xbox.com",
          games_count: 1912,
          image_background:
            "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg",
        },
      },
    ],
    clip: null,
    tags: [
      {
        id: 40847,
        name: "Steam Achievements",
        slug: "steam-achievements",
        language: "eng",
        games_count: 35394,
        image_background:
          "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg",
      },
      {
        id: 7,
        name: "Multiplayer",
        slug: "multiplayer",
        language: "eng",
        games_count: 37026,
        image_background:
          "https://media.rawg.io/media/games/ec3/ec3a7db7b8ab5a71aad622fe7c62632f.jpg",
      },
      {
        id: 40836,
        name: "Full controller support",
        slug: "full-controller-support",
        language: "eng",
        games_count: 16589,
        image_background:
          "https://media.rawg.io/media/games/2ba/2bac0e87cf45e5b508f227d281c9252a.jpg",
      },
      {
        id: 7808,
        name: "steam-trading-cards",
        slug: "steam-trading-cards",
        language: "eng",
        games_count: 7569,
        image_background:
          "https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg",
      },
      {
        id: 18,
        name: "Co-op",
        slug: "co-op",
        language: "eng",
        games_count: 11002,
        image_background:
          "https://media.rawg.io/media/games/55e/55ee6432ac2bf224610fa17e4c652107.jpg",
      },
      {
        id: 411,
        name: "cooperative",
        slug: "cooperative",
        language: "eng",
        games_count: 4663,
        image_background:
          "https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg",
      },
      {
        id: 8,
        name: "First-Person",
        slug: "first-person",
        language: "eng",
        games_count: 30485,
        image_background:
          "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg",
      },
      {
        id: 30,
        name: "FPS",
        slug: "fps",
        language: "eng",
        games_count: 12837,
        image_background:
          "https://media.rawg.io/media/games/49c/49c3dfa4ce2f6f140cc4825868e858cb.jpg",
      },
      {
        id: 9,
        name: "Online Co-Op",
        slug: "online-co-op",
        language: "eng",
        games_count: 5158,
        image_background:
          "https://media.rawg.io/media/games/21c/21cc15d233117c6809ec86870559e105.jpg",
      },
      {
        id: 80,
        name: "Tactical",
        slug: "tactical",
        language: "eng",
        games_count: 4752,
        image_background:
          "https://media.rawg.io/media/games/d4b/d4bcd78873edd9992d93aff9cc8db0c8.jpg",
      },
      {
        id: 11669,
        name: "stats",
        slug: "stats",
        language: "eng",
        games_count: 4799,
        image_background:
          "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg",
      },
      {
        id: 40852,
        name: "Steam Workshop",
        slug: "steam-workshop",
        language: "eng",
        games_count: 1434,
        image_background:
          "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg",
      },
      {
        id: 157,
        name: "PvP",
        slug: "pvp",
        language: "eng",
        games_count: 8622,
        image_background:
          "https://media.rawg.io/media/games/447/4470c1e76f01acfaf5af9c207d1c1c92.jpg",
      },
      {
        id: 62,
        name: "Moddable",
        slug: "moddable",
        language: "eng",
        games_count: 873,
        image_background:
          "https://media.rawg.io/media/games/476/476178ef18ab0534771d099f51cdc694.jpg",
      },
      {
        id: 70,
        name: "War",
        slug: "war",
        language: "eng",
        games_count: 9077,
        image_background:
          "https://media.rawg.io/media/games/283/283e7e600366b0da7021883d27159b27.jpg",
      },
      {
        id: 40837,
        name: "In-App Purchases",
        slug: "in-app-purchases",
        language: "eng",
        games_count: 2421,
        image_background:
          "https://media.rawg.io/media/games/93e/93ee6101e1c943732f06080dddb0fe4c.jpg",
      },
      {
        id: 11,
        name: "Team-Based",
        slug: "team-based",
        language: "eng",
        games_count: 1485,
        image_background:
          "https://media.rawg.io/media/games/1a1/1a17e9b6286edb7e1f1e510110ccb0c0.jpg",
      },
      {
        id: 77,
        name: "Realistic",
        slug: "realistic",
        language: "eng",
        games_count: 5074,
        image_background:
          "https://media.rawg.io/media/games/1a1/1a17e9b6286edb7e1f1e510110ccb0c0.jpg",
      },
      {
        id: 131,
        name: "Fast-Paced",
        slug: "fast-paced",
        language: "eng",
        games_count: 10458,
        image_background:
          "https://media.rawg.io/media/games/abd/abdb7e589f0a8ccd36c0582673120b1e.jpg",
      },
      {
        id: 40856,
        name: "Valve Anti-Cheat enabled",
        slug: "valve-anti-cheat-enabled",
        language: "eng",
        games_count: 105,
        image_background:
          "https://media.rawg.io/media/games/78d/78dfae12fb8c5b16cd78648553071e0a.jpg",
      },
      {
        id: 81,
        name: "Military",
        slug: "military",
        language: "eng",
        games_count: 1712,
        image_background:
          "https://media.rawg.io/media/games/054/0549f1a0a5e782d4e81cdf8d022073fa.jpg",
      },
      {
        id: 170,
        name: "Competitive",
        slug: "competitive",
        language: "eng",
        games_count: 1112,
        image_background:
          "https://media.rawg.io/media/games/6fc/6fcf4cd3b17c288821388e6085bb0fc9.jpg",
      },
      {
        id: 73,
        name: "e-sports",
        slug: "e-sports",
        language: "eng",
        games_count: 80,
        image_background:
          "https://media.rawg.io/media/games/cc7/cc77035eb972f179f5090ee2a0fabd99.jpg",
      },
      {
        id: 245,
        name: "Trading",
        slug: "trading",
        language: "eng",
        games_count: 1065,
        image_background:
          "https://media.rawg.io/media/screenshots/02b/02b2367620982ae26d04ed51d5c35505_apw2ZVh.jpg",
      },
    ],
    esrb_rating: { id: 4, name: "Mature", slug: "mature" },
    short_screenshots: [
      {
        id: -1,
        image:
          "https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg",
      },
      {
        id: 81644,
        image:
          "https://media.rawg.io/media/screenshots/ff1/ff16661bb15f7969b44f6c4118870e44.jpg",
      },
      {
        id: 81645,
        image:
          "https://media.rawg.io/media/screenshots/41b/41bb769d247412eac3336dec934aed72.jpg",
      },
      {
        id: 81646,
        image:
          "https://media.rawg.io/media/screenshots/754/754545acdbf71f56e8902a07c7d20696.jpg",
      },
      {
        id: 81647,
        image:
          "https://media.rawg.io/media/screenshots/fd8/fd873cab4c66db0b8e85d8a66e940074.jpg",
      },
      {
        id: 81648,
        image:
          "https://media.rawg.io/media/screenshots/7db/7db2954f7908b6749c36a5f3c9052f65.jpg",
      },
      {
        id: 81649,
        image:
          "https://media.rawg.io/media/screenshots/337/337a3e98b5933f456a2b37b59fab5f39.jpg",
      },
    ],
  },
  {
    id: 5286,
    slug: "tomb-raider",
    name: "Tomb Raider (2013)",
    released: "2013-03-05",
    tba: false,
    background_image:
      "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg",
    rating: 4.05,
    rating_top: 4,
    ratings: [
      { id: 4, title: "recommended", count: 2347, percent: 60.49 },
      { id: 5, title: "exceptional", count: 990, percent: 25.52 },
      { id: 3, title: "meh", count: 424, percent: 10.93 },
      { id: 1, title: "skip", count: 119, percent: 3.07 },
    ],
    ratings_count: 3851,
    reviews_text_count: 14,
    added: 16230,
    added_by_status: {
      yet: 674,
      owned: 10482,
      beaten: 4171,
      toplay: 260,
      dropped: 534,
      playing: 109,
    },
    metacritic: 86,
    playtime: 10,
    suggestions_count: 645,
    updated: "2024-02-06T03:53:43",
    user_game: null,
    reviews_count: 3880,
    saturated_color: "0f0f0f",
    dominant_color: "0f0f0f",
    platforms: [
      {
        platform: {
          id: 18,
          name: "PlayStation 4",
          slug: "playstation4",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 6762,
          image_background:
            "https://media.rawg.io/media/games/587/587588c64afbff80e6f444eb2e46f9da.jpg",
        },
        released_at: "2013-03-05",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 5,
          name: "macOS",
          slug: "macos",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 103318,
          image_background:
            "https://media.rawg.io/media/games/ee3/ee3e10193aafc3230ba1cae426967d10.jpg",
        },
        released_at: "2013-03-05",
        requirements_en: {
          minimum:
            "Minimum:\r\n\r\nOS: macOS 10.9.1\r\n\r\nProcessor: 2.0GHz Intel or greater\r\n\r\nMemory: 4GB\r\n\r\nGraphics: 512Mb AMD 4850, 512Mb Nvidia 650M, Intel HD4000\r\n\r\nHard Drive: 14GB",
        },
        requirements_ru: null,
      },
      {
        platform: {
          id: 4,
          name: "PC",
          slug: "pc",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 525294,
          image_background:
            "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg",
        },
        released_at: "2013-03-05",
        requirements_en: {
          minimum:
            '<strong>Minimum:</strong><br><ul class="bb_ul"><li><strong>OS:</strong>Windows XP / Windows Vista / Windows 7<br>\t</li><li><strong>Processor:</strong>1.8 GHz Processor<br>\t</li><li><strong>Memory:</strong>512 MB RAM<br>\t</li><li><strong>Graphics:</strong>3D graphics card compatible with DirectX 9<br>\t</li><li><strong>DirectX®:</strong>9.0<br>\t</li><li><strong>Hard Drive:</strong>2 GB HD space</li></ul>',
        },
        requirements_ru: {
          minimum: "i486-100, 8 Мб",
          recommended: "Pentium 166, 16 Мб",
        },
      },
      {
        platform: {
          id: 1,
          name: "Xbox One",
          slug: "xbox-one",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 5585,
          image_background:
            "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg",
        },
        released_at: "2013-03-05",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 14,
          name: "Xbox 360",
          slug: "xbox360",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 2796,
          image_background:
            "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg",
        },
        released_at: "2013-03-05",
        requirements_en: null,
        requirements_ru: null,
      },
      {
        platform: {
          id: 16,
          name: "PlayStation 3",
          slug: "playstation3",
          image: null,
          year_end: null,
          year_start: null,
          games_count: 3168,
          image_background:
            "https://media.rawg.io/media/games/d1a/d1a2e99ade53494c6330a0ed945fe823.jpg",
        },
        released_at: "2013-03-05",
        requirements_en: null,
        requirements_ru: null,
      },
    ],
    parent_platforms: [
      { platform: { id: 1, name: "PC", slug: "pc" } },
      { platform: { id: 2, name: "PlayStation", slug: "playstation" } },
      { platform: { id: 3, name: "Xbox", slug: "xbox" } },
      { platform: { id: 5, name: "Apple Macintosh", slug: "mac" } },
    ],
    genres: [
      {
        id: 4,
        name: "Action",
        slug: "action",
        games_count: 178101,
        image_background:
          "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg",
      },
    ],
    stores: [
      {
        id: 33824,
        store: {
          id: 7,
          name: "Xbox 360 Store",
          slug: "xbox360",
          domain: "marketplace.xbox.com",
          games_count: 1912,
          image_background:
            "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg",
        },
      },
      {
        id: 13151,
        store: {
          id: 1,
          name: "Steam",
          slug: "steam",
          domain: "store.steampowered.com",
          games_count: 88467,
          image_background:
            "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg",
        },
      },
      {
        id: 5640,
        store: {
          id: 3,
          name: "PlayStation Store",
          slug: "playstation-store",
          domain: "store.playstation.com",
          games_count: 7895,
          image_background:
            "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
        },
      },
      {
        id: 218233,
        store: {
          id: 8,
          name: "Google Play",
          slug: "google-play",
          domain: "play.google.com",
          games_count: 17078,
          image_background:
            "https://media.rawg.io/media/games/198/1988a337305e008b41d7f536ce9b73f6.jpg",
        },
      },
      {
        id: 79036,
        store: {
          id: 4,
          name: "App Store",
          slug: "apple-appstore",
          domain: "apps.apple.com",
          games_count: 75404,
          image_background:
            "https://media.rawg.io/media/games/aa3/aa36ba4b486a03ddfaef274fb4f5afd4.jpg",
        },
      },
      {
        id: 713685,
        store: {
          id: 11,
          name: "Epic Games",
          slug: "epic-games",
          domain: "epicgames.com",
          games_count: 1318,
          image_background:
            "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg",
        },
      },
    ],
    clip: null,
    tags: [
      {
        id: 31,
        name: "Singleplayer",
        slug: "singleplayer",
        language: "eng",
        games_count: 217626,
        image_background:
          "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg",
      },
      {
        id: 7,
        name: "Multiplayer",
        slug: "multiplayer",
        language: "eng",
        games_count: 37055,
        image_background:
          "https://media.rawg.io/media/games/6c5/6c55e22185876626881b76c11922b073.jpg",
      },
      {
        id: 40836,
        name: "Full controller support",
        slug: "full-controller-support",
        language: "eng",
        games_count: 16633,
        image_background:
          "https://media.rawg.io/media/games/da1/da1b267764d77221f07a4386b6548e5a.jpg",
      },
      {
        id: 13,
        name: "Atmospheric",
        slug: "atmospheric",
        language: "eng",
        games_count: 31906,
        image_background:
          "https://media.rawg.io/media/games/4a0/4a0a1316102366260e6f38fd2a9cfdce.jpg",
      },
      {
        id: 24,
        name: "RPG",
        slug: "rpg",
        language: "eng",
        games_count: 19735,
        image_background:
          "https://media.rawg.io/media/games/20a/20aa03a10cda45239fe22d035c0ebe64.jpg",
      },
      {
        id: 149,
        name: "Third Person",
        slug: "third-person",
        language: "eng",
        games_count: 10798,
        image_background:
          "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg",
      },
      {
        id: 6,
        name: "Exploration",
        slug: "exploration",
        language: "eng",
        games_count: 21713,
        image_background:
          "https://media.rawg.io/media/games/5bb/5bb55ccb8205aadbb6a144cf6d8963f1.jpg",
      },
      {
        id: 193,
        name: "Classic",
        slug: "classic",
        language: "eng",
        games_count: 1782,
        image_background:
          "https://media.rawg.io/media/games/198/1988a337305e008b41d7f536ce9b73f6.jpg",
      },
      {
        id: 189,
        name: "Female Protagonist",
        slug: "female-protagonist",
        language: "eng",
        games_count: 11762,
        image_background:
          "https://media.rawg.io/media/games/3cf/3cff89996570cf29a10eb9cd967dcf73.jpg",
      },
      {
        id: 15,
        name: "Stealth",
        slug: "stealth",
        language: "eng",
        games_count: 6085,
        image_background:
          "https://media.rawg.io/media/games/364/3642d850efb217c58feab80b8affaa89.jpg",
      },
      {
        id: 69,
        name: "Action-Adventure",
        slug: "action-adventure",
        language: "eng",
        games_count: 15405,
        image_background:
          "https://media.rawg.io/media/games/709/709bf81f874ce5d25d625b37b014cb63.jpg",
      },
      {
        id: 150,
        name: "Third-Person Shooter",
        slug: "third-person-shooter",
        language: "eng",
        games_count: 3252,
        image_background:
          "https://media.rawg.io/media/games/9e5/9e5b274c7e3aa5e30beba31b834b0e7e.jpg",
      },
      {
        id: 74,
        name: "Retro",
        slug: "retro",
        language: "eng",
        games_count: 40977,
        image_background:
          "https://media.rawg.io/media/games/e04/e04963f3ac4c4fa83a1dc0b9231e50db.jpg",
      },
      {
        id: 110,
        name: "Cinematic",
        slug: "cinematic",
        language: "eng",
        games_count: 1858,
        image_background:
          "https://media.rawg.io/media/games/2ad/2ad87a4a69b1104f02435c14c5196095.jpg",
      },
      {
        id: 269,
        name: "Quick-Time Events",
        slug: "quick-time-events",
        language: "eng",
        games_count: 539,
        image_background:
          "https://media.rawg.io/media/screenshots/2b4/2b47c5edfeea2970eeb44d1f8cbd562d.jpg",
      },
      {
        id: 126,
        name: "Dinosaurs",
        slug: "dinosaurs",
        language: "eng",
        games_count: 1682,
        image_background:
          "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg",
      },
      {
        id: 306,
        name: "Lara Croft",
        slug: "lara-croft",
        language: "eng",
        games_count: 14,
        image_background:
          "https://media.rawg.io/media/games/e6b/e6b025951f9a72673f41c0588fb85758.jpg",
      },
    ],
    esrb_rating: { id: 4, name: "Mature", slug: "mature" },
    short_screenshots: [
      {
        id: -1,
        image:
          "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg",
      },
      {
        id: 99160,
        image:
          "https://media.rawg.io/media/screenshots/4f9/4f9d5efdecfb63cb99f1baa4c0ceb3bf.jpg",
      },
      {
        id: 99161,
        image:
          "https://media.rawg.io/media/screenshots/80f/80f373082b2a74da3f9c3fe2b877dcd0.jpg",
      },
      {
        id: 99162,
        image:
          "https://media.rawg.io/media/screenshots/a87/a8733e877f8fbe45e4a727c22a06aa2e.jpg",
      },
      {
        id: 99163,
        image:
          "https://media.rawg.io/media/screenshots/3f9/3f91678c6805a76419fa4ea3a045a909.jpg",
      },
      {
        id: 99164,
        image:
          "https://media.rawg.io/media/screenshots/417/4170bf07be43a8d8249193883f87f1c1.jpg",
      },
      {
        id: 99165,
        image:
          "https://media.rawg.io/media/screenshots/2a4/2a4250f83ad9e959d8b4ca9376ae34ea.jpg",
      },
    ],
  },
];

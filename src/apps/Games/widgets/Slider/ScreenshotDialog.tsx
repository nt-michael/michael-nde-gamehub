import React from "react";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Carousel from "react-material-ui-carousel";
import { Screenshots } from "../../../../configs/type";

interface CustomizedDialogProps {
  open: boolean;
  onclose: () => void;
  images: Screenshots[];
  openingIndex: number;
}

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const ImageCarousel = ({
  images,
  openingIndex,
}: {
  images: Screenshots[];
  openingIndex: number;
}) => {
  // import { Paper, Button } from "@mui/material";

  return (
    <Carousel index={openingIndex} autoPlay>
      {images.map((image) => (
        <img className="carousel-image" key={image.id} src={image.image} />
      ))}
    </Carousel>
  );
};

const CustomizedDialog: React.FC<CustomizedDialogProps> = ({
  open,
  onclose,
  images,
  openingIndex,
}) => {
  return (
    <React.Fragment>
      <BootstrapDialog
        onClose={onclose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth={"lg"}
        fullWidth={true}
      >
        <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          Screenshots
        </DialogTitle>
        <IconButton
          aria-label="close"
          onClick={onclose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent>
          <ImageCarousel images={images} openingIndex={openingIndex} />
        </DialogContent>
      </BootstrapDialog>
    </React.Fragment>
  );
};

export default CustomizedDialog;

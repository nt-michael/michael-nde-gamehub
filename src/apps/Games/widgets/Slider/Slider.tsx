import React, { useEffect, useState } from "react";
import "./slider.scss";
// import { games } from "./games";
import { Game } from "../../../../configs/type";
import { Button, Container, Fade } from "@mui/material";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faStar,
} from "@fortawesome/free-solid-svg-icons";
import SliderLoader from "./SliderLoader";
import CustomizedDialog from "./ScreenshotDialog";
import { useNavigate } from "react-router";

interface SliderProps {
  showDetailsButton?: true;
  games?: Game[];
  singleGame?: Game;
  loading?: boolean;
}

const Slider: React.FC<SliderProps> = ({
  games,
  showDetailsButton,
  loading,
  singleGame,
}) => {
  const [currentGame, setCurrentGame] = useState<Game | null>(null);
  const [showDialog, setShowDialog] = useState<boolean>(false);
  const [dialogIndex, setDialogIndex] = useState<number>(0);
  const [show, setShow] = useState<boolean>(true);
  const navigate = useNavigate();

  useEffect(() => {
    setTimeout(() => {
      if (games) {
        setCurrentGame(games[0]);
      } else {
        if (singleGame) {
          setCurrentGame(singleGame);
        }
      }
    }, 2000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const displayElementsSequentially = (array: Game[]) => {
    let index = 0;

    function displayNextElement() {
      if (index < array.length) {
        hideShow();
        setTimeout(() => {
          setCurrentGame(array[index]);
          showShow();
          index++;
        }, 500); // Add a delay before updating the currentGame
      } else {
        index = 0;
      }
      setTimeout(displayNextElement, 30000);
    }

    displayNextElement();
  };

  useEffect(() => {
    if (games) {
      displayElementsSequentially(games);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    showShow();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentGame]);

  const openDialog = (index: number) => {
    setDialogIndex(index);
    setShowDialog(true);
  };

  const closeDialog = () => {
    setShowDialog(false);
  };

  const showShow = () => {
    setShow(true);
  };

  const hideShow = () => {
    setShow(false);
  };

  return (
    <div className="app-slider">
      <div className="fading-slider-container">
        <div className="slider-wrapper">
          {currentGame && !loading ? (
            <div className={`game-banner`}>
              <Fade in={show}>
                <img
                  src={currentGame.background_image}
                  alt={"cover of" + currentGame.name}
                />
              </Fade>
              <div className="game-info">
                <Fade in={show}>
                  <Container>
                    <div className="content">
                      <h1>{currentGame.name}</h1>
                      <div className="basic-info">
                        <div className="info">
                          <div className="title">Genres</div>
                          <p>
                            {currentGame.genres.map((genre, index) => (
                              <span key={index}>{genre.name}</span>
                            ))}
                          </p>
                        </div>
                        <div className="info">
                          <div className="title">Platform</div>
                          <p>
                            {currentGame.parent_platforms.map(
                              (platform, index) => (
                                <span key={index}>
                                  {platform.platform.name}
                                  {index !==
                                    currentGame.parent_platforms.length - 1 &&
                                    ", "}
                                </span>
                              )
                            )}
                          </p>
                        </div>
                      </div>
                      <div className="basic-info">
                        <div className="info">
                          <div className="title">Publisher</div>
                          <p>{"CD PROJEKT RED"}</p>
                        </div>
                        <div className="info">
                          <div className="title">Stores</div>
                          <p>
                            {currentGame.stores.map((store, index) => (
                              <span key={index}>
                                {store.store.name}
                                {index !== currentGame.stores.length - 1 &&
                                  ", "}
                              </span>
                            ))}
                          </p>
                        </div>
                      </div>
                      <div className="screenshots">
                        <div className="title">Screenshots</div>
                        <div className="screenshots-sub">
                          {currentGame.short_screenshots.map(
                            (screenshot, index) => (
                              <img
                                src={screenshot.image}
                                key={screenshot.id}
                                onClick={() => {
                                  openDialog(index);
                                }}
                              />
                            )
                          )}
                        </div>
                      </div>
                      <div className="actions">
                        <Button
                          onClick={() => navigate(`/app/games/${currentGame.slug}`)}
                          variant="contained"
                          startIcon={<FontAwesomeIcon icon={faPlus} />}
                          disableElevation
                        >
                          View more
                        </Button>
                        <Button variant="contained" disableElevation>
                          <FontAwesomeIcon icon={faStar} />&nbsp;
                          {currentGame.rating}
                        </Button>
                      </div>
                    </div>
                  </Container>
                </Fade>
              </div>
            </div>
          ) : (
            <SliderLoader />
          )}
        </div>
      </div>
      {showDetailsButton && <p>button</p>}
      {currentGame && (
        <CustomizedDialog
          open={showDialog}
          onclose={() => {
            closeDialog();
          }}
          images={currentGame.short_screenshots}
          openingIndex={dialogIndex}
        />
      )}
    </div>
  );
};

export default Slider;

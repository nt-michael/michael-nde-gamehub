// import './game-list.scss'
import Genres from '../Genres/Genres'
import { Autocomplete, Box, Container, Grid, Stack, TextField, Typography } from '@mui/material'
import { Link } from 'react-router-dom';
import GameCard from '../../../widgets/GameCard/GameCard';
import useCombinedStores from '../../../Store';
import { SyntheticEvent, useEffect, useState } from 'react';
import { useGames } from '../../hooks/useGames';
import { useGenres } from '../../hooks/useGenres';
import { usePlatforms } from '../../hooks/usePlatforms';
import { LoadingButton } from '@mui/lab';

const GameList = () => {
    const { orderStore, gameStore, platformStore, genreStore, filterStore } = useCombinedStores();
    const [page, setPage] = useState<number>(1);

    const { data: listGames, isLoading: isLoadingGames, refetch: refetchGames } = useGames(page, filterStore as never);
    const { data: listGenres, refetch: refetchGenres } = useGenres();
    const { data: listPlatform, refetch: refetchPlatforms } = usePlatforms();

    const [isInitLoad, setIsInitLoad] = useState<boolean>(true);
    const [isLoadMore, setIsLoadMore] = useState<boolean>(false);

    const [isInitGenre, setIsInitGenre] = useState<boolean>(true);
    const [isInitPlatform, setIsInitPlatform] = useState<boolean>(true);

    useEffect(() => {
        refetchGames();
        refetchGenres();
        refetchPlatforms();
    }, []);

    useEffect(() => {
        refetchGames();
    }, [filterStore]);

    useEffect(() => {
        if (listGames?.results && isInitLoad) {
            gameStore.init(listGames);
            setIsInitLoad(false);
        }

        if (listGames?.results && !isInitLoad && isLoadMore) {
            gameStore.loadMore(listGames);
            setIsLoadMore(false);

        }

        if (listGames?.results && !isInitLoad && !isLoadMore) {
            gameStore.init(listGames);
        }
    }, [listGames]);

    useEffect(() => {
        if (listGenres?.results && isInitGenre) {
            genreStore.init(listGenres);
            setIsInitGenre(false);
        }
    }, [listGenres]);

    useEffect(() => {
        if (listPlatform?.results && isInitPlatform) {
            platformStore.init(listPlatform);
            setIsInitPlatform(false);
        }
    }, [listPlatform]);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const onChangePlatform = (e: SyntheticEvent<Element, Event>, value: string | any[]) => {
        console.log(e, value);
        if (value.length > 0) {
            const newArr = (value as { label: string; value: number }[]).map((val) => (val.value));
            filterStore.filter("platforms", newArr as never);
            return;
        }
        filterStore.filter("platforms", [] as never)
    }

    const onChangeOrdering = (e: SyntheticEvent<Element, Event>, val: { label: string; value: string; } | null) => {
        console.log(e, val?.value);
        filterStore.filter("ordering", (val?.value) ? val?.value as never : null as never)
    }

    const loadMoreGames = async () => {
        setIsLoadMore(true);
        await setPage(gameStore.page ?? 1);
        refetchGames();
        setPage(1)
    }


    return (
        <Box sx={{ mt: 4 }}>
            <Container className='p-2 mb-10'>
                <Typography variant="h4" className='font-bold text-md hover:text-red-500 transition-colors duration-300'>
                    Genres
                </Typography>
                <Box mt={2}>
                    {genreStore && <Genres />}
                </Box>

                <Box mt={6}>
                    <Stack direction="row" flexWrap='wrap' justifyContent="space-between" alignItems="center">
                        <Typography variant="h4" className='font-bold text-md hover:text-red-500 transition-colors duration-300'>
                            All Games
                        </Typography>
                        <Stack direction="row" flexWrap='wrap' justifyContent="space-between" alignItems="center" gap={4}>
                            {platformStore && platformStore.results && <Stack direction="row" flexWrap='wrap' justifyContent="space-between" alignItems="center" gap={1}>
                                <Typography variant="h6" className='font-bold text-sm hover:text-red-500 transition-colors duration-300'>
                                    Filter by Platforms
                                </Typography>
                                <Autocomplete
                                    disablePortal
                                    multiple
                                    onChange={(e, value) => onChangePlatform(e, value)}
                                    options={platformStore.results.map((platform) => ({ label: platform.name, value: platform.id })) ?? []}
                                    sx={{ width: 200, backgroundColor: "#354250" }}
                                    renderInput={(params) => <TextField placeholder='Platform' {...params} sx={{ color: "white " }} />}
                                />
                            </Stack>}
                            {orderStore && orderStore.options && <Stack direction="row" justifyContent="space-between" alignItems="center" gap={1}>
                                <Typography variant="h6" className='font-bold text-sm hover:text-red-500 transition-colors duration-300'>
                                    Order By
                                </Typography>
                                <Autocomplete
                                    disablePortal
                                    onChange={(e, value) => onChangeOrdering(e, value)}
                                    options={orderStore.options.map((order) => ({ label: order.key, value: order.value }))}
                                    sx={{ width: 200, backgroundColor: "#354250", color: "white !important" }}
                                    renderInput={(params) => <TextField sx={{ color: 'white !important' }} placeholder='Order By' {...params} />}
                                />
                            </Stack>}
                        </Stack>
                    </Stack>
                </Box>
                <Grid container mt={4} spacing={2}>
                    {gameStore && gameStore.results && gameStore.results.map((game, idx) => {
                        return <Grid key={idx} item xs={6} md={3} lg={3}>
                            <Link to={`/app/games/${game?.slug}`} >
                                <GameCard data={game as never} />
                            </Link>
                        </Grid >
                    })}
                    {gameStore && !isInitLoad &&
                        <Box className='p-2 w-full flex justify-center' sx={{ textAlign: 'center' }}>
                            <LoadingButton
                                loading={isLoadMore}
                                variant="contained"
                                color="primary"
                                aria-label="Link database"
                                disabled={isLoadingGames}
                                type="button"
                                size="large"
                                sx={{ width: 'fit-content' }}
                                onClick={loadMoreGames}
                            >
                                Load more
                            </LoadingButton>
                        </Box>}
                </Grid>
            </Container>
        </Box>
    )
}

export default GameList;

import { Grid, ToggleButton } from '@mui/material';
// import './genres.scss';
import useCombinedStores from '../../../Store';
import { useState } from 'react';

const Genres = () => {
    const { genreStore, filterStore } = useCombinedStores();

    const [selected, setSelected] = useState<number[]>([]);

    const toggleGenre = (genre: {
        id: number,
        name: string,
        slug: string,
        games_count: number,
        image_background: string
    }) => {
        // Update the selected genres based on the toggled genre
        const isSelected = selected.includes(genre.id);
        const updatedSelected = isSelected
            ? selected.filter((item) => item !== genre.id)
            : [...selected, genre.id];
        // console.log(updatedSelected);

        setSelected(updatedSelected);
        filterStore.filter("genres", updatedSelected as never);
    };

    return (
        <Grid container wrap='wrap' gap={2}>
            {genreStore && genreStore.results && genreStore.results.map((genre, index: number) => (
                <Grid key={index} item>
                    <ToggleButton
                        value={index}
                        sx={{
                            backgroundColor: selected.includes(genre.id) ? '#DB0041 !important' : '#354250',
                            color: selected.includes(genre.id) ? 'white !important' : 'white',
                            borderRadius: "50px",
                            marginBottom: 2,
                            fontSize: "15px",
                            '&:hover': {
                                backgroundColor: selected.includes(genre.id) ? '#DB0041' : '#0F0F0F',
                                color: 'white',
                            },
                        }}
                        selected={selected.includes(genre.id)}
                        onChange={() => toggleGenre(genre)}
                    >
                        {genre.name}
                    </ToggleButton>
                </Grid>
            ))}
        </Grid>
    );
};

export default Genres;

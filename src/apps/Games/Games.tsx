// import "./games.scss";
import withLoading from "../../Components/layout/withLoading";
import GameList from "./widgets/GameList/GameList";
import Slider from "./widgets/Slider/Slider";
import { useEffect, useState } from "react";
import { useGames } from "./hooks/useGames";
import { Game } from "../../configs/type";
import { FilterPayloadDTO } from "../types";

const GamesComp = () => {
  const [loading, setLoading] = useState<boolean>(true);
  const [games, setGames] = useState<Game[]>([]);

  const sliderFilter: FilterPayloadDTO = {
    search: null,
    ordering: "name",
    genres: [4, 3, 10, 14, 59],
    platforms: null
  }

  const { data: gameList, refetch: refetchGames } = useGames(1, sliderFilter);


  const sampleSize = ([...arr], n = 1) => {
    let m = arr.length;
    while (m) {
      const i = Math.floor(Math.random() * m--);
      [arr[m], arr[i]] = [arr[i], arr[m]];
    }
    return arr.slice(0, n);
  };

  useEffect(() => {
    refetchGames();
  }, []);

  useEffect(() => {
    refetchGames();
    if (gameList) {
      const randomGames = sampleSize(gameList.results, 5);
      setGames(randomGames);
      setLoading(false);
    }
  }, [gameList]);

  return (
    <>
      {games && games.length > 0 && <Slider games={games} loading={loading} />}
      <GameList />
    </>
  );
};
const Games = withLoading(GamesComp);
export default Games;

import withLoading from '../../Components/layout/withLoading';
import './experiment-dev.scss';
import useCombinedStores from '../Store';
import { useEffect, useState } from 'react';
import { useGames } from '../Games/hooks/useGames';
import { usePlatforms } from '../Games/hooks/usePlatforms';
import { useGenres } from '../Games/hooks/useGenres';
import { useSingleGame } from '../SingleGame/hooks/useSingleGame';
import { useGameMedia } from '../SingleGame/hooks/useGameMedia';
import { useGameTrailer } from '../SingleGame/hooks/useGameTrailer';

const ExperimentDevComp = () => {
    const { orderStore, gameStore, platformStore, genreStore, filterStore } = useCombinedStores();
    const [gameSlug] = useState<string>('rise-of-the-tomb-raider');
    const { data: listGames, isLoading: isLoadingGames, refetch: refetchGames } = useGames(gameStore.page ?? 1, filterStore as never);
    const { data: listGenres, isLoading: isLoadingGenres, refetch: refetchGenres } = useGenres();
    const { data: listPlatform, isLoading: isLoadingPlatform, refetch: refetchPlatforms } = usePlatforms();

    const { data: singleGame, isLoading: isLoadingSingle, refetch: refetchSingleGame } = useSingleGame(gameSlug);
    const { data: mediaContent, isLoading: isLoadingMedia, refetch: refetchMedia } = useGameMedia(gameSlug);
    const { data: trailerContent, isLoading: isLoadingTrailer, refetch: refetchTrailer } = useGameTrailer(gameSlug);

    const [isInitLoad, setIsInitLoad] = useState<boolean>(true);
    const [isInitGenre, setIsInitGenre] = useState<boolean>(true);
    const [isInitPlatform, setIsInitPlatform] = useState<boolean>(true);

    useEffect(() => {
        refetchGames();
        refetchGenres();
        refetchPlatforms();
        refetchSingleGame();
    }, []);

    useEffect(() => {
        if (listGames?.results && isInitLoad) {
            gameStore.init(listGames);
            setIsInitLoad(false);
        }
        if (listGames?.results && !isInitLoad) {
            gameStore.loadMore(listGames);
        }
    }, [listGames]);

    useEffect(() => {
        if (listGenres?.results && isInitGenre) {
            genreStore.init(listGenres);
            setIsInitGenre(false);
        }
    }, [listGenres]);

    useEffect(() => {
        if (listPlatform?.results && isInitPlatform) {
            platformStore.init(listPlatform);
            setIsInitPlatform(false);
        }
    }, [listPlatform]);

    useEffect(() => {
        if (singleGame) {
            refetchMedia();
            refetchTrailer();
        }
    }, [singleGame]);



    const loadMoreGames = () => {
        refetchGames({ queryKey: ["useGames", gameStore.page] })
    }

    return (
        <div className="flex gap-5">
            <div className='w-2/5'>
                <h1>ExperimentDev</h1>
                <br />
                <h2 className='text-3xl font-500'>Games List</h2>
                {isLoadingGames && 'Loading games...'}
                {!isLoadingGames && <div className='game-list'>
                    {gameStore &&
                        <div>
                            <span>jfdkdfodjkiov{gameStore?.page}</span>
                            <ol type='1'>
                                {gameStore.results?.map((game, idx) => <li key={idx} className='cursor-pointer hover:underline'>Game: {game.name}</li>)}
                            </ol>
                            {gameStore.page && <button onClick={loadMoreGames}>Load more</button>}
                        </div>
                    }
                </div>}
                <br />
                <h2 className='text-3xl font-500'>Genre List</h2>
                {isLoadingGenres && 'Loading genres...'}
                {!isLoadingGenres && <div className='genre-list'>
                    {genreStore &&
                        <div>
                            <ol type='1'>
                                {genreStore.results?.map((genre, idx) => <li key={idx}>Genre: {genre.name}</li>)}
                            </ol>
                        </div>
                    }
                </div>}
                <br />
                <h2 className='text-3xl font-500'>Platform List</h2>
                {isLoadingPlatform && 'Loading platforms...'}
                {!isLoadingPlatform && <div className='platform-list'>
                    {platformStore &&
                        <div>
                            <ol type='1'>
                                {platformStore.results?.map((platform, idx) => <li key={idx}>Platform: {platform.name} = id: {platform.id}</li>)}
                            </ol>
                        </div>
                    }
                </div>}
                <br />
                <h2 className='text-3xl font-500'>Ordering List</h2>
                {orderStore.options.map((option, idx) => <div key={idx}>key: {option.key}, {option.value}</div>)}
                <button className='text-white' onClick={() => orderStore.sortBy()}>Toggle Sort Direction</button>
            </div>
            <div className="single-game-detail w-3/5 py-10">
                {isLoadingSingle && 'Loading game...'}
                {!isLoadingSingle &&
                    <div className='wrap'>
                        {!singleGame && 'detailGame NOT available'}
                        {singleGame &&
                            <div className="max-w-lg mx-auto bg-white rounded-xl overflow-hidden shadow-md">
                                {/* Main image */}
                                <img className="h-64 w-full object-cover" src={singleGame.background_image} alt="Main Image" />

                                <div className="p-6">
                                    {/* Card Title */}
                                    <h2 className="font-bold text-xl mb-2">{singleGame.name}</h2>

                                    {/* Description */}
                                    <p className="text-gray-700 text-base mb-4">{singleGame.description}</p>

                                    {/* Platforms */}
                                    <div className="mb-4">
                                        <span className="font-semibold">Platforms:</span>
                                        <span className="text-gray-700">{(singleGame?.parent_platforms.map((parent) => `${parent.platform.name}`))?.join(', ')}</span>
                                    </div>

                                    {/* Genres */}
                                    <div className="mb-4">
                                        <span className="font-semibold">Genres:</span>
                                        <span className="text-gray-700">{(singleGame?.genres.map((genre) => `${genre.name}`))?.join(', ')}</span>
                                    </div>

                                    {/* Publishers */}
                                    <div className="mb-4">
                                        <span className="font-semibold">Publishers:</span>
                                        <span className="text-gray-700"> {(singleGame?.publishers.map((publisher) => `${publisher.name}`))?.join(', ')}</span>
                                    </div>

                                    {/* Media Gallery */}
                                    <div className="grid grid-cols-3 gap-4">
                                        {/* Placeholder images for media gallery */}
                                        {isLoadingMedia && 'Loading media...'}
                                        {mediaContent &&
                                            mediaContent.results.map((media, idx) => <img key={idx} className="h-24 w-full object-cover" src={media.image} alt={singleGame.name} />)
                                        }
                                    </div>

                                    {/* Trailer Video */}
                                    <div className="mt-4">
                                        {isLoadingTrailer && 'Loading trailer...'}
                                        {trailerContent && <iframe className="w-full h-64" src={trailerContent?.results[0]?.data.max} frameBorder="0" allowFullScreen></iframe>}
                                    </div>
                                </div>
                            </div>
                        }
                    </div>
                }
            </div>
        </div>
    )
}

const ExperimentDev = withLoading(ExperimentDevComp);
export default ExperimentDev;
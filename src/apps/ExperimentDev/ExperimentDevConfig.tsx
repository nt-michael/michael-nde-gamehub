import { AppRoute } from "../../configs/type";
import ExperimentDev from "./ExperimentDev";

const ExperimentDevConfig: AppRoute = {
  path: "app/dev",
  element: <ExperimentDev />,
};

export default ExperimentDevConfig;
